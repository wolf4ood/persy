extern crate persy;

mod helpers;
use persy::Persy;
use persy::{ValueMode, Value};
use helpers::create_and_drop;

fn create_and_drop_index<F>(test: &str, f: F)
where
    F: FnOnce(&Persy, &str),
{
    create_and_drop(test, |persy| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .create_index::<u8, u8>(&mut tx, "index1", ValueMode::CLUSTER)
            .expect("index created correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        f(persy, "index1");

        let mut tx = persy.begin().expect("begin transaction works");
        persy.drop_index(&mut tx, "index1").expect(
            "index created correctly",
        );
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");
    });
}

#[test]
fn test_create_drop_index() {
    create_and_drop("create_drop_index", |persy| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .create_index::<u8, u8>(&mut tx, "index1", ValueMode::CLUSTER)
            .expect("index created correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");


        let mut tx = persy.begin().expect("begin transaction works");
        persy.drop_index(&mut tx, "index1").expect(
            "index created correctly",
        );
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

    });
}


#[test]
fn test_put_get_index() {
    create_and_drop_index("create_drop_index", |persy, index_name| {
        let mut tx = persy.begin().expect("begin transaction works");
        persy.put::<u8, u8>(&mut tx, index_name, 10, 12).expect(
            "put works correctly",
        );
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect(
            "get works correctly",
        );
        assert_eq!(res, Some(Value::SINGLE(12)));
    });
}


#[test]
fn test_put_remove_index() {
    create_and_drop_index("create_drop_index", |persy, index_name| {

        let mut tx = persy.begin().expect("begin transaction works");
        persy.put::<u8, u8>(&mut tx, index_name, 10, 12).expect(
            "put works correctly",
        );
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect(
            "get works correctly",
        );
        assert_eq!(res, Some(Value::SINGLE(12)));

        let mut tx = persy.begin().expect("begin transaction works");
        persy
            .remove::<u8, u8>(&mut tx, index_name, 10, None)
            .expect("put works correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect(
            "get works correctly",
        );
        assert_eq!(res, None);
    });
}


#[test]
fn test_put_remove_index_one_tx() {
    create_and_drop_index("create_drop_index", |persy, index_name| {

        let mut tx = persy.begin().expect("begin transaction works");
        persy.put::<u8, u8>(&mut tx, index_name, 10, 12).expect(
            "put works correctly",
        );
        persy
            .remove::<u8, u8>(&mut tx, index_name, 10, None)
            .expect("put works correctly");
        let prep = persy.prepare_commit(tx).expect("prepare with index works");
        persy.commit(prep).expect("commit with index works");

        let res = persy.get::<u8, u8>(index_name, &10).expect(
            "get works correctly",
        );
        assert_eq!(res, None);
    });
}
