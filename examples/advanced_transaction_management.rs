extern crate persy;

use std::path::Path;
use persy::{Config, Persy, TxStrategy};

#[derive(Debug)]
enum Error {
    Persy(persy::PersyError),
}

///
/// Example of transaction concurrency management strategy
///
///
fn main() {

    if !Path::new("concurrency.persy").exists() {
        Persy::create("concurrency.persy").expect("create file correctly");
        let persy = Persy::open("concurrency.persy", persy::Config::new()).expect("open file correctly");

        let mut tx = persy.begin().expect("begin tx correctly");
        persy.create_segment(&mut tx, "data").expect(
            "create segment correctly",
        );
        let prepared = persy.prepare_commit(tx).expect("prepare commit correctly");
        persy.commit(prepared).expect("tx commited correctly");
    }
    if !Path::new("concurrency1.persy").exists() {
        Persy::create("concurrency1.persy").expect("create file correctly");
        let persy = Persy::open("concurrency1.persy", persy::Config::new()).expect("open file correctly");

        let mut tx = persy.begin().expect("begin tx correctly");
        persy.create_segment(&mut tx, "data").expect(
            "create segment correctly",
        );
        let prepared = persy.prepare_commit(tx).expect("prepare commit correctly");
        persy.commit(prepared).expect("tx commited correctly");
    }

    {
        let mut config = Config::new();
        config.change_tx_strategy(TxStrategy::LastWin);
        let persy = Persy::open("concurrency.persy", config).expect("open file correctly");

        let mut tx = persy.begin().unwrap();
        let data = String::from("first data").into_bytes();
        let id = persy.insert_record(&mut tx, "data", &data).unwrap();
        let prep = persy.prepare_commit(tx).unwrap();
        persy.commit(prep).unwrap();


        let second_data = String::from("second data").into_bytes();
        let mut tx = persy.begin().unwrap();
        persy
            .update_record(&mut tx, "data", &id, &second_data)
            .unwrap();

        let third_data = String::from("third data").into_bytes();
        let mut tx1 = persy.begin().unwrap();
        persy
            .update_record(&mut tx1, "data", &id, &third_data)
            .unwrap();

        let prep = persy.prepare_commit(tx).unwrap();
        persy.commit(prep).unwrap();

        let prep = persy.prepare_commit(tx1).unwrap();
        persy.commit(prep).unwrap();


        let val = persy.read_record("data", &id).unwrap().unwrap();
        assert_eq!(val, third_data);
    }
    {
        let mut config = Config::new();
        config.change_tx_strategy(TxStrategy::VersionOnRead);
        let persy = Persy::open("concurrency1.persy", config).expect("open file correctly");
        let mut tx = persy.begin().unwrap();
        let data = String::from("first data").into_bytes();
        let id = persy.insert_record(&mut tx, "data", &data).unwrap();
        let prep = persy.prepare_commit(tx).unwrap();
        persy.commit(prep).unwrap();


        let mut tx = persy.begin().unwrap();
        let _persistent_data_can_be_check = persy.read_record_tx(&mut tx, "data", &id).unwrap();
        let second_data = String::from("second data").into_bytes();
        persy
            .update_record(&mut tx, "data", &id, &second_data)
            .unwrap();

        let third_data = String::from("third data").into_bytes();
        let mut tx1 = persy.begin().unwrap();
        persy
            .update_record(&mut tx1, "data", &id, &third_data)
            .unwrap();

        let prep = persy.prepare_commit(tx).unwrap();
        persy.commit(prep).unwrap();

        let to_fail = persy.prepare_commit(tx1);
        assert!(to_fail.is_err());

        let val = persy.read_record("data", &id).unwrap().unwrap();
        assert_eq!(val, second_data);
    }
}

impl From<persy::PersyError> for Error {
    fn from(err: persy::PersyError) -> Error {
        Error::Persy(err)
    }
}
