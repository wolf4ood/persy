//! # Persy - Transactional Persistence Engine
//!
//! Simple single file, durable, paginated, transactional persistence engine, based on copy on write, write
//! ahead log, two phase commit.
//!
//! # Example
//!
//! ```rust
//! use persy::{Persy,Config};
//! //...
//! # use persy::PRes;
//! # fn foo() -> PRes<()> {
//! Persy::create("./open.persy")?;
//! let persy = Persy::open("./open.persy",Config::new())?;
//! let mut tx = persy.begin()?;
//! persy.create_segment(&mut tx, "seg")?;
//! let data = vec![1;20];
//! persy.insert_record(&mut tx, "seg", &data)?;
//! let prepared = persy.prepare_commit(tx)?;
//! persy.commit(prepared)?;
//! for rec in persy.scan_records("seg")? {
//!     assert_eq!(rec.content[0], 20);
//!     //....
//! }
//! # Ok(())
//! # }
//! ```
//!
//!
extern crate byteorder;
extern crate fs2;

mod allocator;
mod address;
mod transaction;
mod discref;
mod journal;
mod config;
mod segment;
mod persy;
mod record_scanner;
mod index;

/// Identifier of a persistent record, can be used for read, update or delete the record
pub type PersyId = persy::RecRef;
/// Transaction container, it include all the changes done in a transaction.
pub type Transaction = transaction::Transaction;
pub type TransactionId = Vec<u8>;
use persy::PersyImpl;
pub use record_scanner::IteratorItem;
pub use record_scanner::RecordScanner;
pub use record_scanner::RecordScannerTx;
pub use persy::TransactionFinalize;
use std::sync::Arc;
pub use config::Config;
pub use config::TxStrategy;
pub use persy::PRes;
pub use persy::PersyError;
pub use index::config::ValueMode;
pub use index::tree::Value;
use std::fs::File;
use index::serialization::{IndexType, IndexSerialization};
use index::keeper::{AddTo, AddToOption, ValueContainer, ValueContainerOption};

/// Main structure to operate persy storage files
///
#[derive(Clone)]
pub struct Persy {
    persy_impl: Arc<PersyImpl>,
}

impl Persy {
    /// Create a new database file.
    ///
    /// # Errors
    /// if the file already exist fail.
    ///
    pub fn create<P: Into<String>>(path: P) -> PRes<()> {
        PersyImpl::create(path)
    }

    /// Create a new database file.
    ///
    /// # Errors
    /// if the file already exist fail.
    ///
    pub fn create_from_file(file: File) -> PRes<()> {
        PersyImpl::create_from_file(file)
    }

    /// Open a database file.
    ///
    /// The file should have been created with [`Persy::create`]
    ///
    ///
    /// # Errors
    ///
    /// if the file does not exist fail.
    ///
    /// [`Persy::create`]: struct.Persy.html#method.create
    pub fn open<P: Into<String>>(path: P, config: Config) -> PRes<Persy> {
        Ok(Persy {
            persy_impl: Arc::new(PersyImpl::open(path, config)?),
        })
    }


    /// Open a database file from a path with a recover function.
    ///
    /// The file should have been created with [`Persy::create`]
    ///
    ///
    /// # Errors
    ///
    /// if the file does not exist fail.
    ///
    /// [`Persy::create`]: struct.Persy.html#method.create
    pub fn open_with_recover<P: Into<String>, C>(path: P, config: Config, recover: C) -> PRes<Persy>
    where
        C: Fn(&TransactionId) -> bool,
    {
        Ok(Persy {
            persy_impl: Arc::new(PersyImpl::open_with_recover(path, config, recover)?),
        })
    }

    /// Open a database file from a direct file handle.
    ///
    /// The file should have been created with [`Persy::create`]
    ///
    ///
    /// # Errors
    ///
    /// if the file does not exist fail.
    ///
    /// [`Persy::create`]: struct.Persy.html#method.create
    pub fn open_from_file(path: File, config: Config) -> PRes<Persy> {
        Ok(Persy {
            persy_impl: Arc::new(PersyImpl::open_from_file(path, config)?),
        })
    }

    /// Open a database file, from a direct file handle and a transaction recover function.
    ///
    /// The file should have been created with [`Persy::create`]
    ///
    ///
    /// # Errors
    ///
    /// if the file does not exist fail.
    ///
    /// [`Persy::create`]: struct.Persy.html#method.create
    pub fn open_from_file_with_recover<C>(path: File, config: Config, recover: C) -> PRes<Persy>
    where
        C: Fn(&TransactionId) -> bool,
    {
        Ok(Persy {
            persy_impl: Arc::new(PersyImpl::open_from_file_with_recover(
                path,
                config,
                recover,
            )?),
        })
    }

    /// Begin a new transaction.
    ///
    /// The transaction isolation level is 'read_commited'.
    /// for commit call [`prepare_commit`] and [`commit`]
    ///
    /// [`prepare_commit`]:struct.Persy.html#method.prepare_commit
    /// [`commit`]:struct.Persy.html#method.commit
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// // ...
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn begin(&self) -> PRes<Transaction> {
        self.persy_impl.begin()
    }


    /// Begin a new transaction specifying and id usable for crash recover.
    ///
    /// The transaction isolation level is 'read_commited'.
    /// for commit call [`prepare_commit`] and [`commit`]
    ///
    /// [`prepare_commit`]:struct.Persy.html#method.prepare_commit
    /// [`commit`]:struct.Persy.html#method.commit
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let tx_id = vec![2;2];
    /// let mut tx = persy.begin_id(tx_id)?;
    /// // ...
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn begin_id(&self, meta_id: TransactionId) -> PRes<Transaction> {
        self.persy_impl.begin_id(meta_id)
    }

    /// Create a new segment with the provided name
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_segment(&mut tx, "my_new_segment")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn create_segment(&self, tx: &mut Transaction, segment: &str) -> PRes<()> {
        self.persy_impl.create_segment(tx, segment)
    }

    /// Drop a existing segment
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.drop_segment(&mut tx, "existing_segment_name")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn drop_segment(&self, tx: &mut Transaction, segment: &str) -> PRes<()> {
        self.persy_impl.drop_segment(tx, segment)
    }

    /// Check if a segment already exist in the storage
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_segment(&mut tx, "my_new_segment")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// assert!(persy.exists_segment("my_new_segment")?);
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn exists_segment(&self, segment: &str) -> PRes<bool> {
        self.persy_impl.exists_segment(segment)
    }

    /// Check if a segment already exist in the storage considering the transaction
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_segment(&mut tx, "my_new_segment")?;
    /// assert!(persy.exists_segment_tx(&mut tx, "my_new_segment")?);
    /// # let prepared = persy.prepare_commit(tx)?;
    /// # persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn exists_segment_tx(&self, tx: &Transaction, segment: &str) -> PRes<bool> {
        self.persy_impl.exists_segment_tx(tx, segment)
    }

    /// create a new record
    ///
    /// This function return an id that can be used by [`read_record`] and [`read_record_tx`],
    /// the record content can be read only with the [`read_record_tx`] till the transaction is committed.
    ///
    /// [`read_record_tx`]:struct.Persy.html#method.read_record_tx
    /// [`read_record`]:struct.Persy.html#method.read_record
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// persy.insert_record(&mut tx, "seg", &data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn insert_record(&self, tx: &mut Transaction, segment: &str, rec: &Vec<u8>) -> PRes<PersyId> {
        self.persy_impl.insert_record(tx, segment, rec)
    }

    /// Read the record content considering eventual in transaction changes.
    ///
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let read = persy.read_record_tx(&mut tx, "seg", &id)?.expect("record exists");
    /// assert_eq!(data,read);
    /// # let prepared = persy.prepare_commit(tx)?;
    /// # persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn read_record_tx(&self, tx: &mut Transaction, segment: &str, id: &PersyId) -> PRes<Option<Vec<u8>>> {
        self.persy_impl.read_record_tx(tx, segment, id)
    }

    /// Read the record content from persistent data.
    ///
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// let read = persy.read_record("seg", &id)?.expect("record exits");
    /// assert_eq!(data,read);
    /// # Ok(())
    /// # }
    /// ```
    pub fn read_record(&self, segment: &str, id: &PersyId) -> PRes<Option<Vec<u8>>> {
        self.persy_impl.read_record(segment, id)
    }


    /// Scan for persistent and in transaction records
    ///
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let mut count = 0;
    /// for found in persy.scan_records_tx(&tx, "seg")? {
    ///     println!("record size:{}",found.content.len());
    ///     count+=1;
    /// }
    /// assert_eq!(count,1);
    /// # Ok(())
    /// # }
    /// ```
    pub fn scan_records_tx<'a>(&'a self, tx: &'a Transaction, segment: &str) -> PRes<RecordScannerTx<'a>> {
        self.persy_impl.scan_records_tx(tx, segment)
    }

    /// Scan a segment for persistent records
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// let mut count = 0;
    /// for found in persy.scan_records("seg")? {
    ///     println!("record size:{}",found.content.len());
    ///     count+=1;
    /// }
    /// assert_eq!(count,1);
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn scan_records(&self, segment: &str) -> PRes<RecordScanner> {
        self.persy_impl.scan_records(segment)
    }

    /// update the record content.
    ///
    ///
    /// This updated content can be read only with the [`read_record_tx`] till the transaction is committed.
    ///
    /// [`read_record_tx`]:struct.Persy.html#method.read_record_tx
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// let new_data = vec![2;20];
    /// persy.update_record(&mut tx, "seg", &id, &new_data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn update_record(&self, tx: &mut Transaction, segment: &str, id: &PersyId, rec: &Vec<u8>) -> PRes<()> {
        self.persy_impl.update_record(tx, segment, id, rec)
    }


    /// delete a record.
    ///
    ///
    /// The record will result deleted only reading it whit [`read_record_tx`] till the transaction is committed.
    ///
    /// [`read_record_tx`]:struct.Persy.html#method.read_record_tx
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// let id = persy.insert_record(&mut tx, "seg", &data)?;
    /// persy.delete_record(&mut tx, "seg", &id)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn delete_record(&self, tx: &mut Transaction, segment: &str, id: &PersyId) -> PRes<()> {
        self.persy_impl.delete_record(tx, segment, id)
    }


    /// Create a new index with the name and the value management mode.
    ///
    /// The create operation require two template arguments that are the types as keys and
    /// values of the index this have to match the following operation on the indexes.
    ///
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn create_index<K, V>(&self, tx: &mut Transaction, index_name: &str, value_mode: ValueMode) -> PRes<()>
    where
        K: Clone + Ord + IndexType,
        V: Clone + IndexType,
    {
        self.persy_impl.create_index::<K, V>(
            tx,
            index_name,
            value_mode,
        )
    }


    /// Drop an existing index.
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.drop_index(&mut tx, "my_new_index")?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn drop_index(&self, tx: &mut Transaction, index_name: &str) -> PRes<()> {
        self.persy_impl.drop_index(tx, index_name)
    }


    /// Put a key value in an index following the value mode strategy.
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// persy.put::<u8,u8>(&mut tx, "my_new_index",10,10)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn put<K, V>(&self, tx: &mut Transaction, index_name: &str, k: K, v: V) -> PRes<()>
    where
        K: Clone + Ord + IndexType,
        V: Clone + IndexType,
        K: AddTo,
        V: AddTo,
        Option<V>: AddToOption,
        ValueContainer: std::convert::From<Vec<K>>,
        ValueContainer: std::convert::From<Vec<V>>,
        ValueContainerOption: std::convert::From<Vec<Option<V>>>,
    {
        self.persy_impl.put::<K, V>(tx, index_name, k, v)
    }

    /// Remove a key and optionally a specific value from an index following the value mode strategy.
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// persy.put::<u8,u8>(&mut tx, "my_new_index",10,10)?;
    /// persy.remove::<u8,u8>(&mut tx, "my_new_index",10,Some(10))?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn remove<K, V>(&self, tx: &mut Transaction, index_name: &str, k: K, v: Option<V>) -> PRes<()>
    where
        K: Clone + Ord + IndexType,
        V: Clone + IndexType,
        K: AddTo,
        V: AddTo,
        Option<V>: AddToOption,
        ValueContainer: std::convert::From<Vec<K>>,
        ValueContainer: std::convert::From<Vec<V>>,
        ValueContainerOption: std::convert::From<Vec<Option<V>>>,
    {
        self.persy_impl.remove::<K, V>(tx, index_name, k, v)
    }


    /// Get a value or a group of values from a key.
    ///
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes,ValueMode, Value};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./data.persy")?;
    /// # let persy = Persy::open("./data.persy",Config::new())?;
    /// # let mut tx = persy.begin()?;
    /// # persy.create_index::<u8,u8>(&mut tx, "my_new_index", ValueMode::CLUSTER)?;
    /// # persy.put::<u8,u8>(&mut tx, "my_new_index",10,10)?;
    /// # let prepared = persy.prepare_commit(tx)?;
    /// # persy.commit(prepared)?;
    /// let val = persy.get::<u8,u8>("my_new_index",&10)?;
    /// if let Some(is_there) = val {
    ///     // A value is actually there
    ///     match is_there {
    ///         Value::SINGLE(actual_value) => {
    ///         },
    ///         Value::CLUSTER(actual_value) => {
    ///         },
    ///     }
    /// }
    /// # Ok(())
    /// # }
    /// ```
    pub fn get<K, V>(&self, index_name: &str, k: &K) -> PRes<Option<Value<V>>>
    where
        K: Clone + Ord + IndexType + IndexSerialization,
        V: Clone + Ord + IndexType + IndexSerialization,
    {
        self.persy_impl.get::<K, V>(index_name, k)
    }

    /// Rollback a not yet prepared transaction.
    ///
    /// All the resources used for eventual insert or update are released.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// let data = vec![1;20];
    /// persy.insert_record(&mut tx, "seg", &data)?;
    /// persy.rollback(tx)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn rollback(&self, tx: Transaction) -> PRes<()> {
        self.persy_impl.rollback(tx)
    }


    /// Prepare to commit a transaction, it will lock all the records involved in the transaction
    /// till a [`commit`] or [`rollback_prepared`] is called.
    ///
    /// [`commit`]:struct.Persy.html#method.commit
    /// [`rollback_prepared`]:struct.Persy.html#method.rollback_prepared
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// # persy.create_segment(&mut tx, "seg")?;
    /// //Do what ever operations on the records
    /// let data = vec![1;20];
    /// persy.insert_record(&mut tx, "seg", &data)?;
    /// let _= persy.prepare_commit(tx)?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn prepare_commit(&self, tx: Transaction) -> PRes<TransactionFinalize> {
        self.persy_impl.prepare_commit(tx)
    }

    /// Rollback a prepared commit.
    ///
    /// All the modification are rolled back and all the used resources are put released
    ///
    ///
    /// # Example
    ///
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// //Do what ever operations on the records
    /// let data = vec![1;20];
    /// persy.insert_record(&mut tx, "seg", &data)?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.rollback_prepared(prepared)?;
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn rollback_prepared(&self, finalizer: TransactionFinalize) -> PRes<()> {
        self.persy_impl.rollback_prepared(finalizer)
    }


    /// Finalize the commit result of a prepared commit.
    ///
    /// All the operation done on the transaction are finalized all the lock released, all the
    /// old resources are released for reuse.
    /// # Example
    /// ```rust
    /// # use persy::{Persy,Config,PRes};
    /// # fn foo() -> PRes<()> {
    /// # Persy::create("./open.persy")?;
    /// # let persy = Persy::open("./open.persy",Config::new())?;
    /// let mut tx = persy.begin()?;
    /// let prepared = persy.prepare_commit(tx)?;
    /// persy.commit(prepared);
    /// # Ok(())
    /// # }
    /// ```
    ///
    pub fn commit(&self, finalizer: TransactionFinalize) -> PRes<()> {
        self.persy_impl.commit(finalizer)
    }
}
