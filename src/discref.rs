
use std::io::{Read, Write, Seek, SeekFrom, Cursor};
use std::io;
use std::fs::File;
use persy::PRes;
use byteorder::{WriteBytesExt, ReadBytesExt, BigEndian};
use std::sync::Mutex;


pub struct DiscRef {
    file: Mutex<File>,
}

pub struct Page {
    buff: Cursor<Vec<u8>>,
    index: u64,
    size: u64,
    exp: u8,
}

pub trait PageSeek {
    fn seek(&mut self, pos: u32) -> PRes<()>;
}

impl Clone for Page {
    fn clone(&self) -> Page {
        let mut data = Cursor::new(self.buff.get_ref().clone());
        data.seek(SeekFrom::Start(2)).unwrap();
        Page {
            buff: data,
            index: self.index,
            size: self.size,
            exp: self.exp,
        }
    }
}

impl Page {
    pub fn new(buff: Cursor<Vec<u8>>, index: u64, exp: u8) -> Page {
        Page {
            buff: buff,
            index: index,
            size: 1 << exp,
            exp: exp,
        }
    }
}

impl Read for Page {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.buff.read(buf)
    }
}

impl Write for Page {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let pre = self.buff.seek(SeekFrom::Current(0))?;
        if pre + (*buf).len() as u64 > self.size {
            panic!(
                "Over page size:{}, data size: {}",
                self.size,
                pre + (*buf).len() as u64
            );
        }
        self.buff.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.buff.flush()
    }
}

impl PageSeek for Page {
    fn seek(&mut self, pos: u32) -> PRes<()> {
        self.buff.seek(SeekFrom::Start(pos as u64 + 2 as u64))?;
        Ok(())
    }
}

impl Page {
    pub fn get_index(&self) -> u64 {
        self.index
    }

    pub fn get_size_exp(&self) -> u8 {
        self.exp
    }

    pub fn set_next_free(&mut self, next: u64) -> PRes<()> {
        let pre = self.buff.seek(SeekFrom::Current(0))?;
        self.buff.seek(SeekFrom::Start(2))?;
        self.buff.write_u64::<BigEndian>(next)?;
        self.buff.seek(SeekFrom::Start(pre))?;
        Ok(())
    }

    pub fn get_next_free(&mut self) -> PRes<u64> {
        let pre = self.buff.seek(SeekFrom::Current(0))?;
        self.buff.seek(SeekFrom::Start(2))?;
        let val = self.buff.read_u64::<BigEndian>()?;
        self.buff.seek(SeekFrom::Start(pre))?;
        Ok(val)
    }

    pub fn reset(&mut self) -> PRes<()> {
        self.buff = Cursor::new(vec![0; self.size as usize]);
        self.buff.write_u8(self.exp)?;
        Ok(())
    }
}

impl DiscRef {
    pub fn new(file: File) -> DiscRef {
        DiscRef { file: Mutex::new(file) }
    }

    pub fn load_page(&self, page: u64) -> PRes<Page> {
        // add 2 to skip the metadata
        let mut ve;
        let exp;
        {
            let ref mut fl = self.file.lock()?;
            fl.seek(SeekFrom::Start(page))?;
            exp = fl.read_u8()?;
            let size = (1 << exp) as u64; //EXP - (size_exp+size_mitigator);
            fl.seek(SeekFrom::Current(-1))?;
            ve = vec![0 as u8; size as usize];
            fl.read_exact(&mut ve[0..size as usize])?;
        }
        let mut cur = Cursor::new(ve);
        cur.seek(SeekFrom::Start(2))?;
        Ok(Page::new(cur, page, exp))
    }

    /// Load a page avoiding to skip base page metadata, used for root page or metadata
    /// manipulation.
    ///
    pub fn load_page_raw(&self, page: u64, size_exp: u8) -> PRes<Page> {
        let mut ve;
        let size;
        {
            let ref mut fl = self.file.lock()?;
            fl.seek(SeekFrom::Start(page))?;
            size = (1 << size_exp) as u64; //EXP - (size_exp+size_mitigator);
            ve = vec![0 as u8; size as usize];
            fl.read_exact(&mut ve[0..size as usize])?;
        }
        Ok(Page::new(Cursor::new(ve), page, size_exp))
    }

    pub fn flush_page(&self, page: &Page) -> PRes<()> {
        let ref mut fl = self.file.lock()?;
        fl.seek(SeekFrom::Start(page.index))?;
        fl.write_all(page.buff.get_ref())?;
        fl.flush()?;
        Ok(())
    }

    /// Create a page without setting metadata, used by root page
    pub fn create_page_raw(&self, exp: u8) -> PRes<u64> {
        let ref mut fl = self.file.lock()?;
        let offset = fl.seek(SeekFrom::End(0))?;
        let size: u64 = (1 << exp) as u64; //EXP - (size_exp+size_mitigator);
        fl.set_len(offset + size)?;
        fl.flush()?;
        Ok(offset)
    }

    pub fn create_page(&self, exp: u8) -> PRes<u64> {
        let ref mut fl = self.file.lock()?;
        let offset = fl.seek(SeekFrom::End(0))?;
        let size: u64 = (1 << exp) as u64; //EXP - (size_exp+size_mitigator);
        fl.write_u8(exp)?; //exp
        fl.write_u8(0)?; //mitigator
        fl.set_len(offset + size)?;
        fl.flush()?;
        Ok(offset)
    }

    pub fn flush(&self) -> PRes<()> {
        Ok(self.file.lock()?.flush()?)
    }
}

#[cfg(test)]
mod tests {
    extern crate tempfile;
    use self::tempfile::Builder;
    use super::DiscRef;
    use byteorder::{WriteBytesExt, ReadBytesExt};


    #[test]
    fn create_load_flush_page() {
        let file = Builder::new()
            .prefix("disc_ref.raw")
            .suffix(".persy")
            .tempfile()
            .unwrap()
            .reopen()
            .unwrap();
        let disc = DiscRef::new(file);
        let page = disc.create_page(5).unwrap();
        let ref mut pg = disc.load_page(page).unwrap();
        disc.flush_page(pg).unwrap();
    }

    #[test]
    fn set_get_next_free() {
        let file = Builder::new()
            .prefix("set_free.raw")
            .suffix(".persy")
            .tempfile()
            .unwrap()
            .reopen()
            .unwrap();
        let disc = DiscRef::new(file);
        let page = disc.create_page(5).unwrap();
        let ref mut pg = disc.load_page(page).unwrap();
        pg.set_next_free(30).unwrap();
        disc.flush_page(pg).unwrap();
        let ref mut pg1 = disc.load_page(page).unwrap();
        let val = pg1.get_next_free().unwrap();
        assert_eq!(val, 30);
    }

    #[test]
    fn get_size_page() {
        let file = Builder::new()
            .prefix("get_size.raw")
            .suffix(".persy")
            .tempfile()
            .unwrap()
            .reopen()
            .unwrap();
        let disc = DiscRef::new(file);
        let page = disc.create_page(5).unwrap();
        let ref mut pg = disc.load_page(page).unwrap();
        let sz = pg.get_size_exp();
        assert_eq!(sz, 5);
    }

    #[test]
    fn write_read_page() {
        let file = Builder::new()
            .prefix("write_read.raw")
            .suffix(".persy")
            .tempfile()
            .unwrap()
            .reopen()
            .unwrap();
        let disc = DiscRef::new(file);
        let page = disc.create_page(5).unwrap();
        {
            let ref mut pg = disc.load_page(page).unwrap();
            pg.write_u8(10).unwrap();
            disc.flush_page(pg).unwrap();
        }
        {
            let ref mut pg = disc.load_page(page).unwrap();
            let va = pg.read_u8().unwrap();
            assert_eq!(va, 10);
            let sz = pg.get_size_exp();
            assert_eq!(sz, 5);
        }
    }

}
