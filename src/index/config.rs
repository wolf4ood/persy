use persy::{RecRef, PRes, PersyError, PersyImpl};
use config::Config;
use transaction::Transaction;
use std::collections::hash_map::HashMap;
use std::collections::hash_map::Entry;
use std::io::{Read, Write, Cursor};
use byteorder::{ReadBytesExt, WriteBytesExt, BigEndian};
use std::sync::{Condvar, Mutex, Arc};
use std::str;
use index::serialization::IndexType;
use index::keeper::IndexSegmentKeeper;

#[derive(Clone, Debug, PartialEq)]
pub enum ValueMode {
    EXCLUSIVE,
    CLUSTER,
    REPLACE,
}

fn format_segment_name(index_name: &str) -> String {
    format!("+_{}", index_name)
}


impl From<u8> for ValueMode {
    fn from(value: u8) -> Self {
        match value {
            1 => ValueMode::EXCLUSIVE,
            2 => ValueMode::CLUSTER,
            3 => ValueMode::REPLACE,
            _ => unreachable!("is impossible to get a value mode from values not 1,2,3"),
        }
    }
}

impl ValueMode {
    fn to_u8(&self) -> u8 {
        match self { 
            ValueMode::EXCLUSIVE => 1,
            ValueMode::CLUSTER => 2,
            ValueMode::REPLACE => 3,
        }
    }
}


struct IndexLock {
    write: bool,
    read_count: u32,
    cond: Arc<Condvar>,
}

impl IndexLock {
    fn new_write() -> IndexLock {
        IndexLock {
            write: true,
            read_count: 0,
            cond: Arc::new(Condvar::new()),
        }
    }

    fn new_read() -> IndexLock {
        IndexLock {
            write: false,
            read_count: 1,
            cond: Arc::new(Condvar::new()),
        }
    }

    fn inc_read(&mut self) {
        self.read_count += 1;
    }

    fn dec_read(&mut self) -> bool {
        self.read_count -= 1;
        self.read_count == 0
    }
}


pub struct Indexes {
    index_locks: Mutex<HashMap<String, IndexLock>>,
    config: Arc<Config>,
}

#[derive(Clone)]
pub struct IndexConfig {
    name: String,
    root: Option<RecRef>,
    key_type: u8,
    value_type: u8,
    page_min: usize,
    page_max: usize,
    value_mode: ValueMode,
}

impl IndexConfig {
    fn serialize(&self, w: &mut Write) -> PRes<()> {
        if let Some(ref root) = self.root {
            w.write_u64::<BigEndian>(root.page)?;
            w.write_u32::<BigEndian>(root.pos)?;
        } else {
            w.write_u64::<BigEndian>(0)?;
            w.write_u32::<BigEndian>(0)?;
        }
        w.write_u8(self.key_type)?;
        w.write_u8(self.value_type)?;
        w.write_u32::<BigEndian>(self.page_min as u32)?;
        w.write_u32::<BigEndian>(self.page_max as u32)?;
        w.write_u8(self.value_mode.to_u8())?;
        w.write_u16::<BigEndian>(self.name.len() as u16)?;
        w.write_all(self.name.as_bytes())?;
        Ok(())
    }
    fn deserialize(r: &mut Read) -> PRes<IndexConfig> {
        let index_root_page = r.read_u64::<BigEndian>()?;
        let index_root_pos = r.read_u32::<BigEndian>()?;
        let key_type = r.read_u8()?;
        let value_type = r.read_u8()?;
        let page_min = r.read_u32::<BigEndian>()? as usize;
        let page_max = r.read_u32::<BigEndian>()? as usize;
        let value_mode = ValueMode::from(r.read_u8()?);

        let name_size = r.read_u16::<BigEndian>()? as usize;
        let mut slice: Vec<u8> = vec![0; name_size];
        r.read_exact(&mut slice)?;
        let name: String = str::from_utf8(&slice[0..name_size])?.into();
        let root = if index_root_page != 0 && index_root_pos != 0 {
            Some(RecRef::new(index_root_page, index_root_pos))
        } else {
            None
        };
        Ok(IndexConfig {
            name: name,
            root: root,
            key_type: key_type,
            value_type: value_type,
            page_min: page_min,
            page_max: page_max,
            value_mode: value_mode,
        })
    }
}

fn error_map(err: PersyError) -> PersyError {
    match err {
        PersyError::SegmentNotFound => {
            return PersyError::IndexNotFound;
        }
        _ => {}
    }
    err
}


impl Indexes {
    pub fn new(config: &Arc<Config>) -> Indexes {
        Indexes {
            index_locks: Mutex::new(HashMap::new()),
            config: config.clone(),
        }
    }

    pub fn create_index<K, V>(p: &PersyImpl, tx: &mut Transaction, name: &str, min: usize, max: usize, value_mode: ValueMode) -> PRes<()>
    where
        K: Clone + Ord + IndexType,
        V: Clone + IndexType,
    {
        if min > max / 2 {
            return Err(PersyError::IndexMinElementsShouldBeAtLeastDoubleOfMax);
        }
        let segment_name = format_segment_name(name);
        p.create_segment(tx, &segment_name)?;
        let cfg = IndexConfig {
            name: name.to_string(),
            root: None,
            key_type: K::get_type_id(),
            value_type: V::get_type_id(),
            page_min: min,
            page_max: max,
            value_mode: value_mode,
        };
        let mut scfg = Vec::new();
        cfg.serialize(&mut scfg)?;
        p.insert_record(tx, &segment_name, &scfg)?;
        Ok(())
    }

    pub fn drop_index(p: &PersyImpl, tx: &mut Transaction, name: &str) -> PRes<()> {
        let segment_name = format_segment_name(name);
        p.drop_segment(tx, &segment_name)
    }

    fn get_index(p: &PersyImpl, name: &str) -> PRes<Option<IndexConfig>> {
        let segment_name = format_segment_name(name);
        if let Some(r) = p.scan_records(&segment_name)
            .map_err(error_map)?
            .into_iter()
            .next()
        {
            Ok(Some(IndexConfig::deserialize(&mut Cursor::new(r.content))?))
        } else {
            Ok(None)
        }
    }
    pub fn update_index_root(p: &PersyImpl, tx: &mut Transaction, name: &str, root: Option<RecRef>) -> PRes<()> {

        let segment_name = format_segment_name(name);
        let (id, mut config) = if let Some(r) = p.scan_records(&segment_name)
            .map_err(error_map)?
            .into_iter()
            .next()
        {
            (r.id, IndexConfig::deserialize(&mut Cursor::new(r.content))?)
        } else {
            return Err(PersyError::IndexNotFound);
        };

        if config.root != root {
            config.root = root;
            let mut scfg = Vec::new();
            config.serialize(&mut scfg)?;
            p.update_record(tx, &segment_name, &id, &scfg)?;
        }
        Ok(())
    }

    pub fn check_and_get_index<K: Clone + Ord + IndexType, V: Clone + IndexType>(p: &PersyImpl, name: &str) -> PRes<IndexConfig> {
        if let Some(index) = Indexes::get_index(p, name)? {
            if index.key_type != K::get_type_id() {
                return Err(PersyError::IndexTypeMismatch(
                    "given key type miss match to persistent key type"
                        .to_string(),
                ));
            }
            if index.value_type != V::get_type_id() {
                return Err(PersyError::IndexTypeMismatch(
                    "given value type miss match to persistent key type"
                        .to_string(),
                ));
            }
            Ok(index)
        } else {
            Err(PersyError::IndexNotFound)
        }
    }

    pub fn check_and_get_index_keeper<'a, K: Clone + Ord + IndexType, V: Clone + IndexType>(
        p: &'a PersyImpl,
        tx: Option<&'a mut Transaction>,
        name: &str,
    ) -> PRes<IndexSegmentKeeper<'a>> {
        let config = Indexes::check_and_get_index::<K, V>(p, name)?;
        Ok(IndexSegmentKeeper::new(
            &format_segment_name(name),
            config.root,
            p,
            tx,
            config.value_mode,
        ))
    }


    pub fn write_lock(&self, indexes: &Vec<String>) -> PRes<()> {
        for index in indexes {
            let seg_lock = IndexLock::new_write();
            loop {
                let mut lock_manager = self.index_locks.lock()?;
                let cond = match lock_manager.entry(index.clone()) {
                    Entry::Occupied(o) => o.get().cond.clone(),
                    Entry::Vacant(v) => {
                        v.insert(seg_lock);
                        break;
                    }
                };
                cond.wait_timeout(
                    lock_manager,
                    self.config.transaction_lock_timeout().clone(),
                )?;
            }
        }
        Ok(())
    }

    pub fn read_lock(&self, index: String) -> PRes<()> {
        loop {
            let mut lock_manager = self.index_locks.lock()?;
            let cond;
            match lock_manager.entry(index.clone()) {
                Entry::Occupied(mut o) => {
                    if o.get().write {
                        cond = o.get().cond.clone();
                    } else {
                        o.get_mut().inc_read();
                        break;
                    }
                }
                Entry::Vacant(v) => {
                    v.insert(IndexLock::new_read());
                    break;
                }
            };
            cond.wait_timeout(
                lock_manager,
                self.config.transaction_lock_timeout().clone(),
            )?;
        }
        Ok(())
    }

    pub fn read_unlock(&self, index: String) -> PRes<()> {
        let mut lock_manager = self.index_locks.lock()?;
        if let Entry::Occupied(mut lock) = lock_manager.entry(index) {
            if lock.get_mut().dec_read() {
                let cond = lock.get().cond.clone();
                lock.remove();
                cond.notify_one();
            }
        }
        Ok(())
    }

    pub fn write_unlock(&self, indexes: &Vec<String>) -> PRes<()> {
        for index in indexes {
            let mut lock_manager = self.index_locks.lock()?;
            if let Some(lock) = lock_manager.remove(index) {
                lock.cond.notify_one();
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::{ValueMode, IndexConfig};
    use std::io::Cursor;
    use persy::RecRef;

    #[test()]
    fn test_config_ser_des() {
        let cfg = IndexConfig {
            name: "abc".to_string(),
            root: None,
            key_type: 1,
            value_type: 1,
            page_min: 10,
            page_max: 30,
            value_mode: ValueMode::REPLACE,
        };

        let mut buff = Vec::new();
        cfg.serialize(&mut Cursor::new(&mut buff)).expect(
            "serialization works",
        );
        let read = IndexConfig::deserialize(&mut Cursor::new(&mut buff)).expect("deserialization works");
        assert_eq!(cfg.name, read.name);
        assert_eq!(cfg.root, read.root);
        assert_eq!(cfg.key_type, read.key_type);
        assert_eq!(cfg.value_type, read.value_type);
        assert_eq!(cfg.page_min, read.page_min);
        assert_eq!(cfg.page_max, read.page_max);
        assert_eq!(cfg.value_mode, read.value_mode);
    }
}
