use persy::RecRef;
use index::keeper::IndexKeeper;
use PRes;
pub type NodeRef = RecRef;
use index::config::ValueMode;
use PersyError;

#[derive(Clone)]
pub enum Node<K, V> {
    NODE(Nodes<K>),
    LEAF(Leaf<K, V>),
}

#[derive(Clone)]
pub struct Nodes<K> {
    pub keys: Vec<K>,
    pub pointers: Vec<NodeRef>,
}

impl<K: Clone + Ord> Nodes<K> {
    pub fn new_from_split(left: NodeRef, values: &Vec<(K, NodeRef)>) -> Nodes<K> {
        let keys = values.iter().map(|z| z.0.clone()).collect();
        let mut pointers: Vec<NodeRef> = values.iter().map(|z| z.1.clone()).collect();
        pointers.insert(0, left);
        Nodes {
            keys: keys,
            pointers: pointers,
        }
    }

    pub fn add(&mut self, pos: usize, k: &K, node_ref: NodeRef) {
        self.keys.insert(pos, k.clone());
        self.pointers.insert(pos + 1, node_ref);
    }

    pub fn find(&self, k: &K) -> PosRef {
        match self.keys.binary_search(k) {
            Ok(index) => PosRef::new(index, self.pointers[index].clone()),
            Err(index) => PosRef::new(index, self.pointers[index].clone()),
        }
    }

    pub fn get(&self, pos: usize) -> NodeRef {
        self.pointers[pos].clone()
    }

    pub fn get_key(&self, pos: usize) -> K {
        self.keys[pos].clone()
    }

    pub fn insert_after(&mut self, pos: usize, values: &mut Vec<(K, NodeRef)>) {
        values.reverse();
        for val in values.iter() {
            self.add(pos, &val.0, val.1.clone());
        }
    }

    pub fn remove(&mut self, pos: usize) -> bool {
        if pos < self.keys.len() {
            self.keys.remove(pos);
            self.pointers.remove(pos + 1);
            true
        } else {
            false
        }
    }

    pub fn len(&self) -> usize {
        self.keys.len()
    }

    pub fn split(&mut self, max: usize) -> Vec<(K, Nodes<K>)> {
        let mut split_result = Vec::new();
        let size = self.keys.len();
        let n_split = size / max;
        let split_offset = size / (n_split + 1) + 1;
        let mut others = self.keys.split_off(split_offset - 1);
        let mut other_pointers = self.pointers.split_off(split_offset);

        while others.len() > max {
            let new = others.split_off(split_offset);
            let new_pointers = other_pointers.split_off(split_offset);
            let key = others.pop().unwrap();
            let leaf = Nodes {
                keys: others,
                pointers: other_pointers,
            };
            split_result.push((key, leaf));
            others = new;
            other_pointers = new_pointers;
        }

        let key = others.pop().unwrap();
        let leaf = Nodes {
            keys: others,
            pointers: other_pointers,
        };
        split_result.push((key, leaf));
        split_result
    }

    pub fn merge_left(&mut self, owner: K, nodes: &mut Nodes<K>) {
        let mut keys = nodes.keys.clone();
        let mut pointers = nodes.pointers.clone();
        nodes.keys.clear();
        nodes.pointers.clear();
        keys.push(owner);
        keys.append(&mut self.keys);
        pointers.append(&mut self.pointers);
        self.keys = keys;
        self.pointers = pointers;
    }

    pub fn merge_right(&mut self, owner: K, nodes: &mut Nodes<K>) {
        self.keys.push(owner);
        self.keys.append(&mut nodes.keys);
        self.pointers.append(&mut nodes.pointers);
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum Value<V> {
    CLUSTER(Vec<V>),
    SINGLE(V),
}

#[derive(Clone)]
pub struct Leaf<K, V> {
    pub entries: Vec<LeafEntry<K, V>>,
}

#[derive(Clone)]
pub struct LeafEntry<K, V> {
    pub key: K,
    pub value: Value<V>,
}

impl<K: Clone + Ord, V: Clone + Ord> Leaf<K, V> {
    pub fn new() -> Leaf<K, V> {
        Leaf { entries: Vec::new() }
    }

    pub fn add(&mut self, pos: usize, k: &K, v: &V, _value_mode: ValueMode) {
        self.entries.insert(
            pos,
            LeafEntry {
                key: k.clone(),
                value: Value::SINGLE(v.clone()),
            },
        );
    }

    pub fn find<'a>(&'a self, k: &K) -> Result<(K, Value<V>), usize> {
        match self.entries.binary_search_by_key(k, |ref n| n.key.clone()) {
            Ok(index) => Ok((
                self.entries[index].key.clone(),
                self.entries[index].value.clone(),
            )),
            Err(index) => Err(index),
        }
    }

    pub fn insert_or_update(&mut self, k: &K, v: &V, value_mode: ValueMode) -> PRes<()> {
        match self.entries.binary_search_by_key(k, |ref n| n.key.clone()) {
            Ok(index) => {
                let entry = &mut self.entries[index];
                match value_mode {
                    ValueMode::REPLACE => {
                        entry.value = Value::SINGLE(v.clone());
                    }
                    ValueMode::EXCLUSIVE => {
                        match &mut entry.value {
                            &mut Value::SINGLE(ref ev) => {
                                if ev != v {
                                    //TODO: add information about the duplicate key
                                    return Err(PersyError::IndexDuplicateKey("".to_string()));
                                }
                            }
                            _ => unreachable!("Exclusive leafs never have cluster values"),
                        }
                    }
                    ValueMode::CLUSTER => {
                        let mut new_value = None;
                        match &mut entry.value {
                            &mut Value::SINGLE(ref ev) => {
                                let mut cl = Vec::new();
                                cl.push(ev.clone());
                                cl.push(v.clone());
                                new_value = Some(Value::CLUSTER(cl));
                            }
                            &mut Value::CLUSTER(ref mut cl) => {
                                cl.push(v.clone());
                            }
                        }
                        if let Some(v) = new_value {
                            entry.value = v;
                        }
                    }
                }
            }
            Err(index) => self.add(index, k, v, value_mode),
        }
        Ok(())
    }

    pub fn remove(&mut self, k: &K, v: &Option<V>) -> bool {
        match self.entries.binary_search_by_key(k, |ref n| n.key.clone()) {
            Ok(index) => {
                if let Some(rv) = v {
                    let mut removed = false;
                    let remove_entry = {
                        let value = &mut self.entries[index].value;
                        let remove_entry = match value {
                            Value::SINGLE(val) => {
                                if val == rv {
                                    removed = true;
                                    true
                                } else {
                                    false
                                }
                            }
                            Value::CLUSTER(ref mut cl) => {
                                if let Ok(index) = cl.binary_search(rv) {
                                    removed = true;
                                    cl.remove(index);
                                }
                                cl.len() == 0
                            }
                        };
                        remove_entry
                    };
                    if remove_entry {
                        self.entries.remove(index);
                    }
                    removed
                } else {
                    self.entries.remove(index);
                    true
                }
            }
            Err(_) => false,
        }
    }

    pub fn len(&self) -> usize {
        self.entries.len()
    }

    pub fn split(&mut self, max: usize) -> Vec<(K, Leaf<K, V>)> {
        let mut split_result = Vec::new();
        let size = self.entries.len();
        let n_split = size / max;
        let split_offset = size / (n_split + 1) + 1;
        let mut others = self.entries.split_off(split_offset);

        while others.len() > max {
            let new = others.split_off(split_offset);
            let key = others[0].key.clone();
            let leaf = Leaf { entries: others };
            split_result.push((key, leaf));
            others = new;
        }

        let key = others[0].key.clone();
        let leaf = Leaf { entries: others };
        split_result.push((key, leaf));
        split_result
    }

    pub fn merge_left(&mut self, leaf: &mut Leaf<K, V>) {
        let mut entries = leaf.entries.clone();
        entries.append(&mut self.entries);
        self.entries = entries;
        leaf.entries.clear();
    }

    pub fn merge_right(&mut self, leaf: &mut Leaf<K, V>) {
        self.entries.append(&mut leaf.entries);
    }
}

pub struct KeyValueAdd<K, V>(pub K, pub V);
pub struct KeyValueRemove<K, V>(pub K, pub Option<V>);


pub trait Index<K: Clone + Ord, V: Clone> {
    fn apply(&mut self, adds: &Vec<KeyValueAdd<K, V>>, removes: &Vec<KeyValueRemove<K, V>>) -> PRes<()>;
    fn get(&mut self, k: &K) -> PRes<Option<Value<V>>>;
}

#[derive(PartialEq, Clone, Debug)]
pub struct PosRef {
    pub pos: usize,
    pub node_ref: NodeRef,
}

impl PosRef {
    fn new(pos: usize, node_ref: NodeRef) -> PosRef {
        PosRef {
            pos: pos,
            node_ref: node_ref.clone(),
        }
    }
}

struct NodeChanged {
    node_ref: NodeRef,
    path: Vec<PosRef>,
}

impl NodeChanged {
    fn new(node_ref: NodeRef, path: Vec<PosRef>) -> NodeChanged {
        NodeChanged {
            node_ref: node_ref,
            path: path,
        }
    }
}

impl<K: Clone + Ord, V: Clone + Ord, T> Index<K, V> for T
where
    T: IndexKeeper<K, V>,
{
    fn apply(&mut self, adds: &Vec<KeyValueAdd<K, V>>, removes: &Vec<KeyValueRemove<K, V>>) -> PRes<()> {
        let mut root = self.get_root()?;
        let mut modified_root = false;
        let mut updates = Vec::new();
        for add in adds {
            if let Some(ref node) = root {
                let mut path = Vec::new();
                let mut cur_node = PosRef::new(0, node.clone());
                loop {
                    match self.load(&cur_node.node_ref)? {
                        Node::NODE(n) => {
                            path.push(cur_node);
                            cur_node = n.find(&add.0);
                        }
                        Node::LEAF(mut leaf) => {
                            leaf.insert_or_update(&add.0, &add.1, self.value_mode())?;
                            self.update(&cur_node.node_ref, Node::LEAF(leaf))?;
                            updates.push(NodeChanged::new(cur_node.node_ref, path));
                            break;
                        }
                    }
                }
            } else {
                let mut leaf = Leaf::new();
                leaf.add(0, &add.0, &add.1, self.value_mode());
                let leaf_ref = self.insert(Node::LEAF(leaf))?;
                // Probably should have been set later after split but if set later it should
                // manage the root creation
                root = Some(leaf_ref);
                modified_root = true;
            }
        }

        for remove in removes {
            if let Some(ref node) = root {
                let mut path = Vec::new();
                let mut cur_node = PosRef::new(0, node.clone());
                loop {
                    match self.load(&cur_node.node_ref)? {
                        Node::NODE(n) => {
                            path.push(cur_node);
                            cur_node = n.find(&remove.0);
                        }
                        Node::LEAF(mut leaf) => {
                            if leaf.remove(&remove.0, &remove.1) {
                                self.update(&cur_node.node_ref, Node::LEAF(leaf))?;
                                updates.push(NodeChanged::new(cur_node.node_ref, path));
                            }
                            break;
                        }
                    }
                }
            }
        }

        let mut last_checked = None;
        let mut parent_updates = Vec::new();
        let mut new_updates = Vec::new();
        for i in 0..updates.len() {
            let update = &updates[i];
            if last_checked != Some(update.node_ref.clone()) {
                last_checked = Some(update.node_ref.clone());
                if let Node::LEAF(mut leaf) = self.load(&update.node_ref)? {
                    if leaf.len() < self.bottom_limit() {
                        let mut parent_path = update.path.clone();
                        let last = parent_path.pop();
                        if let Some(parent_pos) = last {
                            let pos = parent_pos.pos;
                            if let Node::NODE(mut n) = self.load(&parent_pos.node_ref)? {
                                if pos > 0 {
                                    let node_ref = n.get(pos - 1);
                                    if let Node::LEAF(mut dest_merge) = self.load(&node_ref)? {
                                        dest_merge.merge_right(&mut leaf);
                                        self.update(&node_ref, Node::LEAF(dest_merge))?;
                                        new_updates.push((i, NodeChanged::new(node_ref, update.path.clone())));
                                        self.delete(&update.node_ref)?;
                                        n.remove(pos);
                                    } else {
                                        unreachable!("impossible to get a not leaf here");
                                    }
                                } else {
                                    let node_ref = n.get(pos + 1);
                                    if let Node::LEAF(mut source_merge) = self.load(&node_ref)? {
                                        leaf.merge_right(&mut source_merge);
                                        self.update(&update.node_ref, Node::LEAF(leaf))?;
                                        self.delete(&node_ref)?;
                                        n.remove(pos + 1);
                                    } else {
                                        unreachable!("impossible to get a not leaf here");
                                    }
                                }
                                self.update(&parent_pos.node_ref, Node::NODE(n))?;
                                parent_updates.push(NodeChanged::new(parent_pos.node_ref, parent_path))
                            } else {
                                unreachable!("impossible to get a not node here");
                            }
                        } else if leaf.len() == 0 {
                            if let Some(r) = root {
                                self.delete(&r)?;
                                root = None;
                                modified_root = true;
                            }
                        }
                    }
                } else {
                    unreachable!("impossible to get a not leaf here");
                }
            }

        }
        for new_update in new_updates {
            updates.insert(new_update.0, new_update.1);
        }

        for update in &mut updates {
            if last_checked != Some(update.node_ref.clone()) {
                last_checked = Some(update.node_ref.clone());
                if let Node::LEAF(mut leaf) = self.load(&update.node_ref)? {
                    if leaf.len() > self.top_limit() {
                        let splits = leaf.split(self.top_limit());
                        self.update(&update.node_ref, Node::LEAF(leaf))?;
                        let mut ids = Vec::new();
                        for element in splits {
                            let saved_id = self.insert(Node::LEAF(element.1))?;
                            ids.push((element.0.clone(), saved_id));
                        }
                        if let Some(last) = update.path.pop() {
                            if let Node::NODE(mut n) = self.load(&last.node_ref)? {
                                n.insert_after(last.pos, &mut ids);
                                self.update(&last.node_ref, Node::NODE(n))?;
                                parent_updates.push(NodeChanged::new(last.node_ref, update.path.clone()))
                            } else {
                                unreachable!("impossible to get a not node here");
                            }
                        } else {
                            let node = Nodes::new_from_split(update.node_ref.clone(), &ids);
                            let new_root = self.insert(Node::NODE(node))?;
                            root = Some(new_root);
                            modified_root = true;
                        }
                    }
                } else {
                    unreachable!("impossible to get a not node here");
                }
            }

        }

        loop {

            let mut last_checked = None;
            let mut updates = parent_updates;
            if updates.len() == 0 {
                break;
            }
            parent_updates = Vec::new();
            let mut new_updates = Vec::new();
            for i in 0..updates.len() {
                let update = &updates[i];
                if last_checked != Some(update.node_ref.clone()) {
                    last_checked = Some(update.node_ref.clone());
                    if let Node::NODE(mut node) = self.load(&update.node_ref)? {
                        if node.len() < self.bottom_limit() {
                            let mut parent_path = update.path.clone();
                            let last = parent_path.pop();
                            if let Some(parent_pos) = last {
                                let pos = parent_pos.pos;
                                if let Node::NODE(mut n) = self.load(&parent_pos.node_ref)? {
                                    if pos > 0 {
                                        let node_ref = n.get(pos - 1);
                                        if let Node::NODE(mut dest_merge) = self.load(&node_ref)? {
                                            dest_merge.merge_right(n.get_key(pos), &mut node);
                                            self.update(&node_ref, Node::NODE(dest_merge))?;
                                            new_updates.push((i, NodeChanged::new(node_ref, update.path.clone())));
                                            self.delete(&update.node_ref)?;
                                            n.remove(pos);
                                        } else {
                                            unreachable!("impossible to get a not node here");
                                        }
                                    } else {
                                        let node_ref = n.get(pos + 1);
                                        if let Node::NODE(mut source_merge) = self.load(&node_ref)? {
                                            node.merge_right(n.get_key(pos + 1), &mut source_merge);
                                            self.update(&update.node_ref, Node::NODE(node))?;
                                            self.delete(&node_ref)?;
                                            n.remove(pos + 1);
                                        } else {
                                            unreachable!("impossible to get a not node here");
                                        }
                                    }
                                    self.update(&parent_pos.node_ref, Node::NODE(n))?;
                                    parent_updates.push(NodeChanged::new(parent_pos.node_ref, parent_path))
                                } else {
                                    unreachable!("impossible to get a not node here");
                                }
                            } else if node.len() == 0 {
                                if let Some(r) = root {
                                    self.delete(&r)?;
                                    root = None;
                                    modified_root = true;
                                }
                            }
                        }
                    } else {
                        unreachable!("impossible to get a not node here");
                    }
                }
            }

            for new_update in new_updates {
                updates.insert(new_update.0, new_update.1);
            }

            for update in &mut updates {
                if last_checked != Some(update.node_ref.clone()) {
                    last_checked = Some(update.node_ref.clone());
                    if let Node::NODE(mut node) = self.load(&update.node_ref)? {
                        if node.len() > self.top_limit() {
                            let splits = node.split(self.top_limit());
                            self.update(&update.node_ref, Node::NODE(node))?;
                            let mut ids = Vec::new();
                            for element in splits {
                                let saved_id = self.insert(Node::NODE(element.1))?;
                                ids.push((element.0.clone(), saved_id));
                            }
                            if let Some(last) = update.path.pop() {
                                if let Node::NODE(mut n) = self.load(&last.node_ref)? {
                                    n.insert_after(last.pos, &mut ids);
                                    self.update(&last.node_ref, Node::NODE(n))?;
                                    parent_updates.push(NodeChanged::new(last.node_ref, update.path.clone()))
                                } else {
                                    unreachable!("impossible to get a not node here");
                                }
                            } else {
                                let node = Nodes::new_from_split(update.node_ref.clone(), &ids);
                                let new_root = self.insert(Node::NODE(node))?;
                                root = Some(new_root);
                                modified_root = true;
                            }
                        }
                    } else {
                        unreachable!("impossible to get a not node here");
                    }
                }

            }
        }

        if modified_root {
            self.set_root(root)?;
        }

        Ok(())
    }

    fn get(&mut self, k: &K) -> PRes<Option<Value<V>>> {
        if let Some(node) = self.get_root()? {
            let mut cur_node = node;
            loop {
                match self.load(&cur_node)? {
                    Node::NODE(n) => {
                        let found = n.find(k);
                        cur_node = found.node_ref;
                    }
                    Node::LEAF(mut leaf) => {
                        if let Ok(el) = leaf.find(k) {
                            break Ok(Some(el.1.clone()));
                        } else {
                            break Ok(None);
                        }
                    }
                }
            }
        } else {
            Ok(None)
        }
    }
}

#[cfg(test)]
mod tests {
    extern crate rand;
    use persy::RecRef;
    use super::{IndexKeeper, Index, Node, Nodes, Leaf, PRes, KeyValueAdd, KeyValueRemove, NodeRef, PosRef, ValueMode, Value};
    use self::rand::random;
    use std::collections::HashMap;

    fn random_pointer() -> NodeRef {
        RecRef::new(random::<u64>(), random::<u32>())
    }

    #[test()]
    fn simple_node_add_test() {
        let val1 = random_pointer();
        let val2 = random_pointer();
        let val3 = random_pointer();
        let val4 = random_pointer();
        let val5 = random_pointer();
        let val6 = random_pointer();
        let mut node = Nodes::new_from_split(val1, &vec![(0, val2)]);
        let pos = node.find(&2).pos;
        node.add(pos, &2, val3.clone());
        let pos = node.find(&5).pos;
        node.add(pos, &5, val4);
        let pos = node.find(&6).pos;
        node.add(pos, &6, val5);
        let pos = node.find(&4).pos;
        node.add(pos, &4, val6.clone());

        let found = node.find(&4);
        assert_eq!(found.pos, 2);
        //If i search for 4 i get the one on the left of 4 so the value of 2 that is val3
        assert_eq!(found.node_ref, val3);

        let found = node.find(&5);
        assert_eq!(found.pos, 3);
        //If i search for 5 i get the one on the left of 5 so the value of 4 that is val6
        assert_eq!(found.node_ref, val6);

        let found = node.find(&3);
        //If i search for a value that do not exist i get the position of the value at is right
        //that is value 4 position 2
        assert_eq!(found.pos, 2);
        //If i search for 3 i get the value at the left of 4 that is val3
        assert_eq!(found.node_ref, val3);
    }

    #[test()]
    fn simple_leaf_insert_test() {
        let mut leaf = Leaf::new();
        for n in 0..50 {
            leaf.insert_or_update(&n, &n, ValueMode::REPLACE).expect(
                "insert is ok",
            );
        }
        let res = leaf.find(&10);
        assert_eq!(Ok((10, Value::SINGLE(10))), res);

        let res = leaf.find(&60);
        assert_eq!(Err(50), res);
    }

    #[test()]
    fn simple_leaf_cluster_insert_test() {
        let mut leaf = Leaf::new();
        leaf.insert_or_update(&10, &1, ValueMode::CLUSTER).expect(
            "insert is ok",
        );
        leaf.insert_or_update(&10, &2, ValueMode::CLUSTER).expect(
            "insert is ok",
        );
        let res = leaf.find(&10);
        assert_eq!(Ok((10, Value::CLUSTER(vec![1, 2]))), res);
    }


    #[test()]
    fn leaf_cluster_remove_test() {
        let mut leaf = Leaf::new();
        leaf.insert_or_update(&10, &1, ValueMode::CLUSTER).expect(
            "insert is ok",
        );
        leaf.insert_or_update(&10, &2, ValueMode::CLUSTER).expect(
            "insert is ok",
        );
        assert!(leaf.remove(&10, &Some(2)));
        let res = leaf.find(&10);
        assert_eq!(Ok((10, Value::CLUSTER(vec![1]))), res);
    }

    #[test()]
    fn leaf_cluster_remove_not_exist_value_test() {
        let mut leaf = Leaf::new();
        leaf.insert_or_update(&10, &1, ValueMode::CLUSTER).expect(
            "insert is ok",
        );
        leaf.insert_or_update(&10, &2, ValueMode::CLUSTER).expect(
            "insert is ok",
        );
        assert!(!leaf.remove(&10, &Some(10)));
        let res = leaf.find(&10);
        assert_eq!(Ok((10, Value::CLUSTER(vec![1, 2]))), res);
    }

    #[test()]
    fn leaf_single_remove_not_exist_value_test() {
        let mut leaf = Leaf::new();
        leaf.insert_or_update(&10, &1, ValueMode::EXCLUSIVE)
            .expect("insert is ok");
        assert!(!leaf.remove(&10, &Some(10)));
        let res = leaf.find(&10);
        assert_eq!(Ok((10, Value::SINGLE(1))), res);
    }


    #[test()]
    fn leaf_duplicate_key_test() {
        let mut leaf = Leaf::new();
        leaf.insert_or_update(&10, &1, ValueMode::EXCLUSIVE)
            .expect("insert is ok");
        let res = leaf.insert_or_update(&10, &2, ValueMode::EXCLUSIVE);
        assert!(res.is_err());
    }


    #[test()]
    fn test_leaf_split() {
        let mut leaf = Leaf::new();

        for n in 0..103 {
            leaf.insert_or_update(&n, &n, ValueMode::REPLACE).expect(
                "insert is ok",
            );
        }

        let res = leaf.split(21);
        assert_eq!(leaf.len(), 21);
        assert_eq!(res[0].1.len(), 21);
        assert_eq!(res[1].1.len(), 21);
        assert_eq!(res[2].1.len(), 21);
        assert_eq!(res[3].1.len(), 19);
    }

    #[test()]
    fn test_node_split() {
        let mut node = Nodes::new_from_split(random_pointer(), &vec![(0, random_pointer())]);
        for n in 1..103 {
            let pos = node.find(&n).pos;
            node.add(pos, &n, random_pointer());
        }

        let res = node.split(21);
        assert_eq!(node.len(), 20);
        assert_eq!(node.pointers.len(), 21);
        assert_eq!(res[0].1.len(), 20);
        assert_eq!(res[0].1.pointers.len(), 21);
        assert_eq!(res[1].1.len(), 20);
        assert_eq!(res[1].1.pointers.len(), 21);
        assert_eq!(res[2].1.len(), 20);
        assert_eq!(res[2].1.pointers.len(), 21);
        assert_eq!(res[3].1.len(), 19);
        assert_eq!(res[3].1.pointers.len(), 20);
    }

    #[test()]
    fn test_remove_from_leaf() {
        let mut leaf = Leaf::new();
        for n in 0..50 {
            leaf.insert_or_update(&n, &n, ValueMode::REPLACE).expect(
                "insert is ok",
            );
        }
        assert!(leaf.remove(&10, &Some(10)));
        assert!(!leaf.remove(&100, &Some(100)));
        assert_eq!(leaf.len(), 49);
        let res = leaf.find(&10);
        assert_eq!(Err(10), res);
    }

    #[test()]
    fn test_remove_from_node() {
        //TODO: check why the remove of 10 make to point to 9
        let mut node = Nodes::new_from_split(random_pointer(), &vec![(0, random_pointer())]);
        let mut keep = None;
        for n in 1..50 {
            let pos = node.find(&n).pos;
            let point = random_pointer();
            if n == 9 {
                keep = Some(point.clone());
            }
            node.add(pos, &n, point);
        }
        let pos = node.find(&10).pos;
        node.remove(pos);
        assert_eq!(node.len(), 49);
        let res = node.find(&10);
        assert_eq!(PosRef::new(10, keep.unwrap()), res);

    }

    #[test()]
    fn test_merge_leaf() {
        let mut leaf = Leaf::new();
        let mut leaf2 = Leaf::new();
        for n in 0..20 {
            leaf.insert_or_update(&n, &n, ValueMode::REPLACE).expect(
                "insert is ok",
            );
        }

        for n in 20..40 {
            leaf2.insert_or_update(&n, &n, ValueMode::REPLACE).expect(
                "insert is ok",
            );
        }
        leaf.merge_right(&mut leaf2);
        assert_eq!(leaf.len(), 40);
        assert_eq!(leaf2.len(), 0);
        let res = leaf.find(&35);
        assert_eq!(res, Ok((35, Value::SINGLE(35))));

        let mut leaf = Leaf::new();
        let mut leaf2 = Leaf::new();
        for n in 20..40 {
            leaf.insert_or_update(&n, &n, ValueMode::REPLACE).expect(
                "insert is ok",
            );
        }

        for n in 0..20 {
            leaf2.insert_or_update(&n, &n, ValueMode::REPLACE).expect(
                "insert is ok",
            );
        }
        leaf.merge_left(&mut leaf2);
        assert_eq!(leaf.len(), 40);
        assert_eq!(leaf2.len(), 0);
        let res = leaf.find(&35);
        assert_eq!(res, Ok((35, Value::SINGLE(35))));
    }

    #[test()]
    fn test_merge_nodes() {
        let mut node = Nodes::new_from_split(random_pointer(), &vec![(0, random_pointer())]);
        for n in 1..20 {
            let pos = node.find(&n).pos;
            let point = random_pointer();
            node.add(pos, &n, point);
        }

        let mut node2 = Nodes::new_from_split(random_pointer(), &vec![(21, random_pointer())]);
        let mut keep = None;
        for n in 22..40 {
            let pos = node2.find(&n).pos;
            let point = random_pointer();
            if n == 25 {
                keep = Some(point.clone());
            }
            node2.add(pos, &n, point);
        }

        node.merge_right(20, &mut node2);
        assert_eq!(node.len(), 40);
        assert_eq!(node2.len(), 0);
        let res = node.find(&26);
        assert_eq!(PosRef::new(26, keep.unwrap()), res);



        let mut node = Nodes::new_from_split(random_pointer(), &vec![(21, random_pointer())]);
        let mut keep = None;
        for n in 22..40 {
            let pos = node.find(&n).pos;
            let point = random_pointer();
            if n == 25 {
                keep = Some(point.clone());
            }
            node.add(pos, &n, point);
        }

        let mut node2 = Nodes::new_from_split(random_pointer(), &vec![(0, random_pointer())]);
        for n in 1..20 {
            let pos = node2.find(&n).pos;
            let point = random_pointer();
            node2.add(pos, &n, point);
        }

        node.merge_left(20, &mut node2);
        assert_eq!(node.len(), 40);
        assert_eq!(node2.len(), 0);
        let res = node.find(&26);
        assert_eq!(PosRef::new(26, keep.unwrap()), res);

    }

    struct MockIndexKeeper<K: Clone + Ord, V: Clone> {
        store: HashMap<NodeRef, Node<K, V>>,
        root: Option<NodeRef>,
    }

    impl<K: Clone + Ord, V: Clone> MockIndexKeeper<K, V> {
        fn new() -> MockIndexKeeper<K, V> {
            MockIndexKeeper {
                store: HashMap::new(),
                root: None,
            }
        }
    }

    impl<K: Clone + Ord, V: Clone> IndexKeeper<K, V> for MockIndexKeeper<K, V> {
        fn load(&mut self, node: &NodeRef) -> PRes<Node<K, V>> {
            Ok(self.store.get(&node).unwrap().clone())
        }
        fn insert(&mut self, node: Node<K, V>) -> PRes<NodeRef> {
            let node_ref = random_pointer();
            self.store.insert(node_ref.clone(), node.clone());
            Ok(node_ref)
        }
        fn update(&mut self, node_ref: &NodeRef, node: Node<K, V>) -> PRes<()> {
            self.store.insert(node_ref.clone(), node);
            Ok(())
        }
        fn delete(&mut self, node: &NodeRef) -> PRes<()> {
            self.store.remove(&node);
            Ok(())
        }
        fn get_root(&self) -> PRes<Option<NodeRef>> {
            Ok(self.root.clone())
        }
        fn set_root(&mut self, root: Option<NodeRef>) -> PRes<()> {
            Ok(self.root = root)
        }
        fn bottom_limit(&self) -> usize {
            10
        }
        fn top_limit(&self) -> usize {
            30
        }

        fn value_mode(&self) -> ValueMode {
            ValueMode::REPLACE
        }
    }

    #[test()]
    fn test_simple_add() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        changes.push(KeyValueAdd(1, 1));
        changes.push(KeyValueAdd(2, 2));
        changes.push(KeyValueAdd(3, 4));
        keeper.apply(&changes, &Vec::new()).unwrap();
        assert_eq!(keeper.get(&2), Ok(Some(Value::SINGLE(2))));
    }


    #[test()]
    fn test_many_add() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        for i in 0..200 {
            changes.push(KeyValueAdd(i, i));
        }
        keeper.apply(&changes, &Vec::new()).unwrap();
        assert_eq!(keeper.get(&2), Ok(Some(Value::SINGLE(2))));
        assert_eq!(keeper.get(&100), Ok(Some(Value::SINGLE(100))));
        assert_eq!(keeper.get(&201), Ok(None));
    }

    #[test()]
    fn test_many_add_multiple_times() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        for n in 0..8 {
            for i in 0..20 {
                changes.push(KeyValueAdd(i, i));
            }
            keeper.apply(&changes, &Vec::new()).unwrap();
            assert_eq!(keeper.get(&(n + 2)), Ok(Some(Value::SINGLE(n + 2))));
        }
    }


    #[test()]
    fn test_simple_add_remove() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        changes.push(KeyValueAdd(1, 1));
        changes.push(KeyValueAdd(2, 2));
        changes.push(KeyValueAdd(3, 4));
        keeper.apply(&changes, &Vec::new()).unwrap();
        assert_eq!(keeper.get(&2), Ok(Some(Value::SINGLE(2))));

        let mut changes = Vec::new();
        changes.push(KeyValueRemove(1, Some(1)));
        changes.push(KeyValueRemove(2, Some(2)));
        changes.push(KeyValueRemove(3, Some(4)));
        keeper.apply(&Vec::new(), &changes).unwrap();
        assert_eq!(keeper.get(&2), Ok(None));
    }


    #[test()]
    fn test_many_add_remove() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        for i in 0..200 {
            changes.push(KeyValueAdd(i, i));
        }
        keeper.apply(&changes, &Vec::new()).unwrap();
        assert_eq!(keeper.get(&2), Ok(Some(Value::SINGLE(2))));
        assert_eq!(keeper.get(&100), Ok(Some(Value::SINGLE(100))));
        assert_eq!(keeper.get(&201), Ok(None));
        let mut changes = Vec::new();
        for i in 0..200 {
            changes.push(KeyValueRemove(i, Some(i)));
        }
        keeper.apply(&Vec::new(), &changes).unwrap();
        assert_eq!(keeper.get(&2), Ok(None));
        assert_eq!(keeper.get(&100), Ok(None));
    }

    #[test()]
    fn test_many_add_remove_multiple_times() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        let mut rchanges = Vec::new();
        for n in 0..8 {
            for i in 0..20 {
                changes.push(KeyValueAdd(i, i));
                rchanges.push(KeyValueRemove(i, Some(i)));
            }
            keeper.apply(&changes, &Vec::new()).unwrap();
            assert_eq!(keeper.get(&(n + 2)), Ok(Some(Value::SINGLE(n + 2))));
            keeper.apply(&Vec::new(), &rchanges).unwrap();
            assert_eq!(keeper.get(&(n + 2)), Ok(None));
        }
    }

    #[test()]
    #[ignore()]
    fn test_a_lot_add_remove_multiple_times() {
        let mut keeper = MockIndexKeeper::<u32, u32>::new();
        let mut changes = Vec::new();
        let mut remove = Vec::new();
        for n in 0..30 {
            for i in 0..200 {
                changes.push(KeyValueAdd(i * n, i * n));
                if i % 2 == 0 {
                    remove.push(KeyValueRemove(i * n, Some(i * n)));
                }
            }
            keeper.apply(&changes, &Vec::new()).unwrap();
            assert_eq!(keeper.get(&(2 * n)), Ok(Some(Value::SINGLE(2 * n))));
            assert_eq!(keeper.get(&(100 * n)), Ok(Some(Value::SINGLE(100 * n))));
            assert_eq!(keeper.get(&20001), Ok(None));
            keeper.apply(&Vec::new(), &remove).unwrap();
            assert_eq!(keeper.get(&(2 * n)), Ok(None));
            assert_eq!(keeper.get(&(100 * n)), Ok(None));
        }
    }

}
