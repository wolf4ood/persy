use index::tree::{Node, NodeRef};
use persy::{PersyImpl, PRes};
use transaction::Transaction;
use index::serialization::{IndexSerialization, serialize, deserialize, IndexType};
use index::config::ValueMode;
use index::tree::Index;
use index::config::Indexes;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use index::tree::{KeyValueAdd, KeyValueRemove};
use persy::RecRef;

#[derive(Clone)]
pub enum ValueContainer {
    U8(Vec<u8>),
    U16(Vec<u16>),
    U32(Vec<u32>),
    U64(Vec<u64>),
    I8(Vec<i8>),
    I16(Vec<i16>),
    I32(Vec<i32>),
    I64(Vec<i64>),
    STRING(Vec<String>),
    RECREF(Vec<RecRef>),
}


#[derive(Clone)]
pub enum ValueContainerOption {
    U8(Vec<Option<u8>>),
    U16(Vec<Option<u16>>),
    U32(Vec<Option<u32>>),
    U64(Vec<Option<u64>>),
    I8(Vec<Option<i8>>),
    I16(Vec<Option<i16>>),
    I32(Vec<Option<i32>>),
    I64(Vec<Option<i64>>),
    STRING(Vec<Option<String>>),
    RECREF(Vec<Option<RecRef>>),
}

pub trait AddTo {
    fn add_to(self, vc: &mut ValueContainer);
}

pub trait AddToOption {
    fn add_to_option(self, vc: &mut ValueContainerOption);
}

fn apply_to_index<K, V>(
    persy: &PersyImpl,
    tx: &mut Transaction,
    index_name: &String,
    keys: &Vec<K>,
    rkeys: &Vec<K>,
    values: &Vec<V>,
    rvalues: &Vec<Option<V>>,
) -> PRes<()>
where
    K: IndexType + Ord + Clone + IndexSerialization,
    V: IndexType + Ord + Clone + IndexSerialization,
{
    debug_assert_eq!(keys.len(), values.len());
    debug_assert_eq!(rkeys.len(), rvalues.len());
    let mut to_add = Vec::new();
    let mut to_remove = Vec::new();
    for i in 0..keys.len() {
        to_add.push(KeyValueAdd(keys[i].clone(), values[i].clone()));
    }
    for i in 0..rkeys.len() {
        to_remove.push(KeyValueRemove(rkeys[i].clone(), rvalues[i].clone()));
    }
    to_add.sort_by(|x, z| x.0.cmp(&z.0));
    to_remove.sort_by(|x, z| x.0.cmp(&z.0));
    let root;
    {
        let mut index = Indexes::check_and_get_index_keeper::<K, V>(persy, Some(tx), index_name)?;
        index.apply(&to_add, &to_remove)?;
        root = IndexKeeper::<K, V>::get_root(&index)?;
    }
    Indexes::update_index_root(persy, tx, index_name, root)?;
    Ok(())
}

fn kapplier(
    keys: &ValueContainer,
    values: &ValueContainer,
    rkeys: &ValueContainer,
    rvalues: &ValueContainerOption,
    index_name: &String,
    persy: &PersyImpl,
    tx: &mut Transaction,
) -> PRes<()> {
    match (keys, rkeys) {
        (ValueContainer::U8(k), ValueContainer::U8(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        (ValueContainer::U16(k), ValueContainer::U16(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        (ValueContainer::U32(k), ValueContainer::U32(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        (ValueContainer::U64(k), ValueContainer::U64(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        (ValueContainer::I8(k), ValueContainer::I8(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        (ValueContainer::I16(k), ValueContainer::I16(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        (ValueContainer::I32(k), ValueContainer::I32(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        (ValueContainer::I64(k), ValueContainer::I64(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        (ValueContainer::STRING(k), ValueContainer::STRING(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        (ValueContainer::RECREF(k), ValueContainer::RECREF(rk)) => vapplier(values, rvalues, k, rk, index_name, persy, tx),
        _ => {
            panic!("this combination are not allowed by previous validation");
        }
    }
}
fn vapplier<K>(
    values: &ValueContainer,
    rvalues: &ValueContainerOption,
    k: &Vec<K>,
    rk: &Vec<K>,
    index_name: &String,
    persy: &PersyImpl,
    tx: &mut Transaction,
) -> PRes<()>
where
    K: IndexType + Ord + Clone + IndexSerialization,
{
    match (values, rvalues) {
        (ValueContainer::U8(v), ValueContainerOption::U8(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        (ValueContainer::U16(v), ValueContainerOption::U16(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        (ValueContainer::U32(v), ValueContainerOption::U32(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        (ValueContainer::U64(v), ValueContainerOption::U64(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        (ValueContainer::I8(v), ValueContainerOption::I8(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        (ValueContainer::I16(v), ValueContainerOption::I16(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        (ValueContainer::I32(v), ValueContainerOption::I32(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        (ValueContainer::I64(v), ValueContainerOption::I64(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        (ValueContainer::STRING(v), ValueContainerOption::STRING(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        (ValueContainer::RECREF(v), ValueContainerOption::RECREF(rv)) => apply_to_index(persy, tx, index_name, k, rk, v, rv),
        _ => {
            panic!("this combination are not allowed by previous validation");
        }
    }
}


impl AddTo for u8 {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::U8(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddTo for u16 {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::U16(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddTo for u32 {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::U32(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddTo for u64 {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::U64(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddTo for i8 {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::I8(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddTo for i16 {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::I16(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddTo for i32 {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::I32(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddTo for i64 {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::I64(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddTo for String {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::STRING(ref mut v) => {
                v.push(self.clone());
            }
            _ => {}
        }
    }
}

impl AddTo for RecRef {
    fn add_to(self, vc: &mut ValueContainer) {
        match vc {
            ValueContainer::RECREF(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}


impl From<Vec<u8>> for ValueContainer {
    fn from(v: Vec<u8>) -> ValueContainer {
        ValueContainer::U8(v)
    }
}

impl From<Vec<u16>> for ValueContainer {
    fn from(v: Vec<u16>) -> ValueContainer {
        ValueContainer::U16(v)
    }
}

impl From<Vec<u32>> for ValueContainer {
    fn from(v: Vec<u32>) -> ValueContainer {
        ValueContainer::U32(v)
    }
}

impl From<Vec<u64>> for ValueContainer {
    fn from(v: Vec<u64>) -> ValueContainer {
        ValueContainer::U64(v)
    }
}


impl From<Vec<i8>> for ValueContainer {
    fn from(v: Vec<i8>) -> ValueContainer {
        ValueContainer::I8(v)
    }
}

impl From<Vec<i16>> for ValueContainer {
    fn from(v: Vec<i16>) -> ValueContainer {
        ValueContainer::I16(v)
    }
}

impl From<Vec<i32>> for ValueContainer {
    fn from(v: Vec<i32>) -> ValueContainer {
        ValueContainer::I32(v)
    }
}

impl From<Vec<i64>> for ValueContainer {
    fn from(v: Vec<i64>) -> ValueContainer {
        ValueContainer::I64(v)
    }
}


impl From<Vec<String>> for ValueContainer {
    fn from(v: Vec<String>) -> ValueContainer {
        ValueContainer::STRING(v)
    }
}

impl From<Vec<RecRef>> for ValueContainer {
    fn from(v: Vec<RecRef>) -> ValueContainer {
        ValueContainer::RECREF(v)
    }
}

impl AddToOption for Option<u8> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::U8(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddToOption for Option<u16> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::U16(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddToOption for Option<u32> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::U32(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddToOption for Option<u64> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::U64(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddToOption for Option<i8> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::I8(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddToOption for Option<i16> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::I16(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddToOption for Option<i32> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::I32(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddToOption for Option<i64> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::I64(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl AddToOption for Option<String> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::STRING(ref mut v) => {
                v.push(self.clone());
            }
            _ => {}
        }
    }
}

impl AddToOption for Option<RecRef> {
    fn add_to_option(self, vc: &mut ValueContainerOption) {
        match vc {
            ValueContainerOption::RECREF(ref mut v) => {
                v.push(self);
            }
            _ => {}
        }
    }
}

impl From<Vec<Option<u8>>> for ValueContainerOption {
    fn from(v: Vec<Option<u8>>) -> ValueContainerOption {
        ValueContainerOption::U8(v)
    }
}

impl From<Vec<Option<u16>>> for ValueContainerOption {
    fn from(v: Vec<Option<u16>>) -> ValueContainerOption {
        ValueContainerOption::U16(v)
    }
}

impl From<Vec<Option<u32>>> for ValueContainerOption {
    fn from(v: Vec<Option<u32>>) -> ValueContainerOption {
        ValueContainerOption::U32(v)
    }
}

impl From<Vec<Option<u64>>> for ValueContainerOption {
    fn from(v: Vec<Option<u64>>) -> ValueContainerOption {
        ValueContainerOption::U64(v)
    }
}


impl From<Vec<Option<i8>>> for ValueContainerOption {
    fn from(v: Vec<Option<i8>>) -> ValueContainerOption {
        ValueContainerOption::I8(v)
    }
}

impl From<Vec<Option<i16>>> for ValueContainerOption {
    fn from(v: Vec<Option<i16>>) -> ValueContainerOption {
        ValueContainerOption::I16(v)
    }
}

impl From<Vec<Option<i32>>> for ValueContainerOption {
    fn from(v: Vec<Option<i32>>) -> ValueContainerOption {
        ValueContainerOption::I32(v)
    }
}

impl From<Vec<Option<i64>>> for ValueContainerOption {
    fn from(v: Vec<Option<i64>>) -> ValueContainerOption {
        ValueContainerOption::I64(v)
    }
}


impl From<Vec<Option<String>>> for ValueContainerOption {
    fn from(v: Vec<Option<String>>) -> ValueContainerOption {
        ValueContainerOption::STRING(v)
    }
}

impl From<Vec<Option<RecRef>>> for ValueContainerOption {
    fn from(v: Vec<Option<RecRef>>) -> ValueContainerOption {
        ValueContainerOption::RECREF(v)
    }
}



pub struct IndexTransactionKeeper {
    indexes_changes: HashMap<String, (ValueContainer, ValueContainer, ValueContainer, ValueContainerOption)>,
}

impl IndexTransactionKeeper {
    pub fn new() -> IndexTransactionKeeper {
        IndexTransactionKeeper { indexes_changes: HashMap::new() }
    }

    pub fn put<K, V>(&mut self, index: &String, k: K, v: V)
    where
        K: AddTo,
        V: AddTo,
        Option<V>: AddToOption,
        ValueContainer: std::convert::From<Vec<K>>,
        ValueContainer: std::convert::From<Vec<V>>,
        ValueContainerOption: std::convert::From<Vec<Option<V>>>,
    {
        match self.indexes_changes.entry(index.clone()) {
            Entry::Occupied(ref mut o) => {
                k.add_to(&mut o.get_mut().0);
                v.add_to(&mut o.get_mut().1);
            }
            Entry::Vacant(va) => {
                let e = (
                    ValueContainer::from(vec![k]),
                    ValueContainer::from(vec![v]),
                    ValueContainer::from(Vec::<K>::new()),
                    ValueContainerOption::from(Vec::<Option<V>>::new()),
                );
                va.insert(e);
            }
        }
    }

    pub fn remove<K, V>(&mut self, index: &String, k: K, v: Option<V>)
    where
        K: AddTo,
        V: AddTo,
        Option<V>: AddToOption,
        ValueContainer: std::convert::From<Vec<K>>,
        ValueContainer: std::convert::From<Vec<V>>,
        ValueContainerOption: std::convert::From<Vec<Option<V>>>,
    {
        match self.indexes_changes.entry(index.clone()) {
            Entry::Occupied(ref mut o) => {
                k.add_to(&mut o.get_mut().2);
                v.add_to_option(&mut o.get_mut().3);
            }
            Entry::Vacant(va) => {
                let e = (
                    ValueContainer::from(Vec::<K>::new()),
                    ValueContainer::from(Vec::<V>::new()),
                    ValueContainer::from(vec![k]),
                    ValueContainerOption::from(vec![v]),
                );
                va.insert(e);
            }
        }
    }

    pub fn apply(&self, persy: &PersyImpl, tx: &mut Transaction) -> PRes<()> {
        for (index, values) in &self.indexes_changes {
            kapplier(&values.0, &values.1, &values.2, &values.3, index, persy, tx)?;
        }
        Ok(())
    }

    pub fn changed_indexes(&self) -> Vec<String> {
        self.indexes_changes.keys().map(|x| x.clone()).collect()
    }
}

pub trait IndexKeeper<K, V> {
    fn load(&mut self, node: &NodeRef) -> PRes<Node<K, V>>;
    fn insert(&mut self, node: Node<K, V>) -> PRes<NodeRef>;
    fn update(&mut self, node_ref: &NodeRef, node: Node<K, V>) -> PRes<()>;
    fn delete(&mut self, node: &NodeRef) -> PRes<()>;
    fn get_root(&self) -> PRes<Option<NodeRef>>;
    fn set_root(&mut self, root: Option<NodeRef>) -> PRes<()>;
    fn bottom_limit(&self) -> usize;
    fn top_limit(&self) -> usize;
    fn value_mode(&self) -> ValueMode;
}


pub struct IndexSegmentKeeper<'a> {
    segment: String,
    root: Option<NodeRef>,
    store: &'a PersyImpl,
    tx: Option<&'a mut Transaction>,
    value_mode: ValueMode,
}

impl<'a> IndexSegmentKeeper<'a> {
    pub fn new(
        segment: &String,
        root: Option<NodeRef>,
        store: &'a PersyImpl,
        tx: Option<&'a mut Transaction>,
        value_mode: ValueMode,
    ) -> IndexSegmentKeeper<'a> {
        IndexSegmentKeeper {
            segment: segment.clone(),
            root: root,
            store: store,
            tx: tx,
            value_mode: value_mode,
        }
    }
}

impl<'a, K: Clone + Ord + IndexSerialization, V: Clone + IndexSerialization> IndexKeeper<K, V> for IndexSegmentKeeper<'a> {
    fn load(&mut self, node: &NodeRef) -> PRes<Node<K, V>> {
        if let Some(ref mut tx) = self.tx {
            Ok(deserialize(
                self.store
                    .read_record_tx(tx, &self.segment, &node)?
                    .as_ref()
                    .unwrap(),
            )?)
        } else {
            Ok(deserialize(
                self.store
                    .read_record(&self.segment, &node)?
                    .as_ref()
                    .unwrap(),
            )?)
        }
    }
    fn insert(&mut self, node: Node<K, V>) -> PRes<NodeRef> {
        if let Some(ref mut tx) = self.tx {
            let node_ref = self.store.insert_record(
                tx,
                &self.segment,
                &serialize(node)?,
            )?;
            Ok(node_ref)
        } else {
            panic!("should never get here");
        }
    }
    fn update(&mut self, node_ref: &NodeRef, node: Node<K, V>) -> PRes<()> {
        if let Some(ref mut tx) = self.tx {
            self.store.update_record(
                tx,
                &self.segment,
                &node_ref,
                &serialize(node)?,
            )?;
            Ok(())
        } else {
            panic!("should never get here");
        }
    }
    fn delete(&mut self, node: &NodeRef) -> PRes<()> {
        if let Some(ref mut tx) = self.tx {
            self.store.delete_record(tx, &self.segment, &node)?;
            Ok(())
        } else {
            panic!("should not get here");
        }
    }
    fn get_root(&self) -> PRes<Option<NodeRef>> {
        Ok(self.root.clone())
    }
    fn set_root(&mut self, root: Option<NodeRef>) -> PRes<()> {
        Ok(self.root = root)
    }
    fn bottom_limit(&self) -> usize {
        10
    }
    fn top_limit(&self) -> usize {
        30
    }
    fn value_mode(&self) -> ValueMode {
        self.value_mode.clone()
    }
}
