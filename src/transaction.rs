
use persy::{RecRef, PRes, PersyError, PersyImpl};
use journal::{JournalId, Journal};
use allocator::Allocator;
use address::Address;
use std::collections::HashSet;
use std::collections::HashMap;
use std::vec;
use config::TxStrategy;
use index::keeper::{IndexTransactionKeeper, ValueContainer, ValueContainerOption, AddTo, AddToOption};
use index::config::Indexes;

#[derive(Clone)]
pub struct InsertRecord {
    pub segment: u32,
    pub recref: RecRef,
    pub record_page: u64,
}

#[derive(Clone)]
pub struct UpdateRecord {
    pub segment: u32,
    pub recref: RecRef,
    pub record_page: u64,
    pub record_old_page: u64,
    pub version: u16,
}

#[derive(Clone)]
pub struct ReadRecord {
    pub segment: u32,
    pub recref: RecRef,
    pub version: u16,
}

#[derive(Clone)]
pub struct DeleteRecord {
    pub segment: u32,
    pub recref: RecRef,
    pub record_old_page: u64,
    pub version: u16,
}

#[derive(Clone)]
pub struct CreateSegment {
    pub name: String,
    pub segment_id: u32,
}

#[derive(Clone)]
pub struct DropSegment {
    pub name: String,
    pub segment_id: u32,
}

#[derive(Clone)]
pub struct FreedRecordPages {
    pub pages: Vec<(u32, u64)>,
}

pub struct PrepareCommit {}

pub struct Commit {}

pub struct Rollback {}
pub struct Metadata {
    pub strategy: TxStrategy,
    pub meta_id: Vec<u8>,
}

pub enum SegmentOperation {
    CREATE(CreateSegment),
    DROP(DropSegment),
}


pub struct Transaction {
    strategy: TxStrategy,
    meta_id: Vec<u8>,
    id: JournalId,
    inserted: Vec<InsertRecord>,
    updated: Vec<UpdateRecord>,
    deleted: Vec<DeleteRecord>,
    read: HashMap<RecRef, ReadRecord>,
    segments_operations: Vec<SegmentOperation>,
    segs_created_names: HashSet<String>,
    segs_dropped_names: HashSet<String>,
    segs_created: HashSet<u32>,
    segs_dropped: HashSet<u32>,
    segs_updated: HashSet<u32>,
    freed_pages: Option<FreedRecordPages>,
    indexes: Option<IndexTransactionKeeper>,
    locked_indexes: Option<Vec<String>>,
}

pub enum TxRead {
    RECORD((u64, u16)),
    DELETED,
    NONE,
}

pub enum TxSegCheck {
    CREATED(u32),
    DROPPED,
    NONE,
}

pub struct TransactionInsertScanner<'a> {
    tx: &'a Transaction,
    segment: u32,
}

pub struct TransactionInsertIterator {
    iter: vec::IntoIter<InsertRecord>,
    segment: u32,
}

impl<'a> IntoIterator for TransactionInsertScanner<'a> {
    type Item = RecRef;
    type IntoIter = TransactionInsertIterator;

    fn into_iter(self) -> Self::IntoIter {
        let iter: vec::IntoIter<InsertRecord> = self.tx.inserted.clone().into_iter();
        TransactionInsertIterator {
            iter: iter,
            segment: self.segment,
        }
    }
}

impl Iterator for TransactionInsertIterator {
    type Item = RecRef;
    fn next(&mut self) -> Option<RecRef> {
        loop {
            let next = self.iter.next();
            if let Some(rec) = next {
                if rec.segment == self.segment {
                    return Some(rec.recref);
                }
            } else {
                return None;
            }
        }
    }
}

impl Transaction {
    pub fn new(journal: &Journal, strategy: &TxStrategy, meta_id: Vec<u8>) -> PRes<Transaction> {
        let id = journal.start()?;
        journal.log(&Metadata::new(strategy, meta_id.clone()), &id)?;
        Ok(Transaction {
            strategy: strategy.clone(),
            meta_id: meta_id,
            id: id,
            inserted: Vec::<InsertRecord>::new(),
            updated: Vec::<UpdateRecord>::new(),
            deleted: Vec::<DeleteRecord>::new(),
            read: HashMap::new(),
            segments_operations: Vec::<SegmentOperation>::new(),
            segs_created_names: HashSet::<String>::new(),
            segs_dropped_names: HashSet::<String>::new(),
            segs_created: HashSet::<u32>::new(),
            segs_dropped: HashSet::<u32>::new(),
            segs_updated: HashSet::<u32>::new(),
            freed_pages: None,
            indexes: Some(IndexTransactionKeeper::new()),
            locked_indexes: None,
        })
    }
    pub fn recover(id: JournalId) -> Transaction {
        Transaction {
            strategy: TxStrategy::LastWin,
            meta_id: Vec::<u8>::new(),
            id: id,
            inserted: Vec::<InsertRecord>::new(),
            updated: Vec::<UpdateRecord>::new(),
            deleted: Vec::<DeleteRecord>::new(),
            read: HashMap::new(),
            segments_operations: Vec::<SegmentOperation>::new(),
            segs_created_names: HashSet::<String>::new(),
            segs_dropped_names: HashSet::<String>::new(),
            segs_created: HashSet::<u32>::new(),
            segs_dropped: HashSet::<u32>::new(),
            segs_updated: HashSet::<u32>::new(),
            freed_pages: None,
            indexes: Some(IndexTransactionKeeper::new()),
            locked_indexes: None,
        }
    }

    pub fn exists_segment(&self, segment: &String) -> TxSegCheck {
        if self.segs_created_names.contains(segment) {
            for a in &self.segments_operations {
                match a {
                    &SegmentOperation::CREATE(ref c) => {
                        return TxSegCheck::CREATED(c.segment_id);
                    }
                    _ => {}
                }
            }
        } else if self.segs_dropped_names.contains(segment) {
            return TxSegCheck::DROPPED;
        }
        TxSegCheck::NONE
    }

    pub fn add_create_segment(&mut self, journal: &Journal, name: &str, segment_id: u32) -> PRes<()> {
        let create = CreateSegment::new(name, segment_id);

        journal.log(&create, &self.id)?;
        self.segments_operations.push(
            SegmentOperation::CREATE(create),
        );
        self.segs_created.insert(segment_id);
        self.segs_created_names.insert(name.into());
        Ok(())
    }

    pub fn recover_add(&mut self, create: &CreateSegment) {
        self.segments_operations.push(SegmentOperation::CREATE(
            create.clone(),
        ));
        self.segs_created.insert(create.segment_id);
        self.segs_created_names.insert(create.name.clone());
    }

    pub fn add_drop_segment(&mut self, journal: &Journal, name: &str, segment_id: u32) -> PRes<()> {
        if self.segs_created_names.contains(name.into()) {
            Err(PersyError::CannotDropSegmentCreatedInTx)
        } else {
            let drop = DropSegment::new(name, segment_id);
            journal.log(&drop, &self.id)?;
            self.segments_operations.push(SegmentOperation::DROP(drop));
            self.segs_dropped.insert(segment_id);
            self.segs_dropped_names.insert(name.into());
            Ok(())
        }
    }

    pub fn recover_drop(&mut self, drop: &DropSegment) {
        self.segments_operations.push(
            SegmentOperation::DROP(drop.clone()),
        );
        self.segs_dropped.insert(drop.segment_id);
        self.segs_dropped_names.insert(drop.name.clone());
    }

    pub fn add_read(&mut self, journal: &Journal, segment: u32, recref: &RecRef, version: u16) -> PRes<()> {
        if self.strategy == TxStrategy::VersionOnRead {
            let read = ReadRecord::new(segment, recref, version);
            journal.log(&read, &self.id)?;
            self.read.insert(recref.clone(), read);
        }
        Ok(())
    }

    pub fn recover_read(&mut self, read: &ReadRecord) {
        self.read.insert(read.recref.clone(), read.clone());
    }

    pub fn add_insert(&mut self, journal: &Journal, segment: u32, rec_ref: &RecRef, record: u64) -> PRes<()> {
        self.segs_updated.insert(segment);
        let insert = InsertRecord::new(segment, rec_ref, record);

        journal.log(&insert, &self.id)?;
        self.inserted.push(insert);
        Ok(())
    }

    pub fn recover_insert(&mut self, insert: &InsertRecord) {
        self.segs_updated.insert(insert.segment);
        self.inserted.push(insert.clone());
    }

    pub fn add_update(&mut self, journal: &Journal, segment: u32, rec_ref: &RecRef, record: u64, old_record: u64, version: u16) -> PRes<()> {
        self.segs_updated.insert(segment);
        let update = UpdateRecord::new(segment, rec_ref, record, old_record, version);
        journal.log(&update, &self.id)?;
        self.updated.push(update);
        Ok(())
    }

    pub fn recover_update(&mut self, update: &UpdateRecord) {
        self.segs_updated.insert(update.segment.clone());
        self.updated.push(update.clone());
    }

    pub fn add_delete(&mut self, journal: &Journal, segment: u32, rec_ref: &RecRef, old_record: u64, version: u16) -> PRes<()> {
        self.segs_updated.insert(segment.clone());
        let delete = DeleteRecord::new(segment, rec_ref, old_record, version);
        journal.log(&delete, &self.id)?;
        self.deleted.push(delete);
        Ok(())
    }

    pub fn add_put<K, V>(&mut self, index: &String, k: K, v: V)
    where
        ValueContainer: std::convert::From<Vec<K>>,
        K: AddTo,
        ValueContainer: std::convert::From<Vec<V>>,
        ValueContainerOption: std::convert::From<Vec<Option<V>>>,
        V: AddTo,
        Option<V>: AddToOption,
    {
        if let Some(ref mut indexes) = self.indexes {
            indexes.put(index, k, v);
        }
    }

    pub fn add_remove<K, V>(&mut self, index: &String, k: K, v: Option<V>)
    where
        ValueContainer: std::convert::From<Vec<K>>,
        ValueContainerOption: std::convert::From<Vec<Option<V>>>,
        K: AddTo,
        ValueContainer: std::convert::From<Vec<V>>,
        Option<V>: AddToOption,
        V: AddTo,
    {
        if let Some(ref mut indexes) = self.indexes {
            indexes.remove(index, k, v);
        }
    }

    pub fn recover_delete(&mut self, delete: &DeleteRecord) {
        self.segs_updated.insert(delete.segment.clone());
        self.deleted.push(delete.clone());
    }

    pub fn scan_insert<'a>(&'a self, seg: u32) -> TransactionInsertScanner<'a> {
        TransactionInsertScanner {
            tx: self,
            segment: seg,
        }
    }


    pub fn read(&self, rec_ref: &RecRef) -> TxRead {

        for ele in &self.deleted {
            if ele.recref.page == rec_ref.page && ele.recref.pos == rec_ref.pos {
                return TxRead::DELETED;
            }
        }
        if let Some(ele) = self.updated.iter().rev().find(|ele| {
            ele.recref.page == rec_ref.page && ele.recref.pos == rec_ref.pos
        })
        {
            return TxRead::RECORD((ele.record_page, ele.version));

        }
        for ele in &self.inserted {
            if ele.recref.page == rec_ref.page && ele.recref.pos == rec_ref.pos {
                return TxRead::RECORD((ele.record_page, 1));
            }
        }
        TxRead::NONE
    }

    pub fn recover_prepare_commit(&mut self, journal: &Journal, address: &Address, allocator: &Allocator) -> PRes<()> {
        let (records, crt_upd_segs, dropped_segs) = self.coll_locks();
        let check_version = self.strategy != TxStrategy::LastWin;
        match address.acquire_locks(&records, &crt_upd_segs, &dropped_segs, check_version) {
            Ok(_) => Ok(()),
            Err(x) => {
                self.recover_rollback(journal, address, allocator)?;
                Err(x)
            }

        }

    }

    pub fn prepare_commit(
        mut self,
        journal: &Journal,
        address: &Address,
        indexes: &Indexes,
        persy_impl: &PersyImpl,
        allocator: &Allocator,
    ) -> PRes<Transaction> {

        let ind = self.indexes;
        self.indexes = None;
        if let Some(ind_change) = ind {
            let to_lock = ind_change.changed_indexes();
            //TODO: manage failure of lock acquire
            indexes.write_lock(&to_lock)?;
            self.locked_indexes = Some(to_lock);
            ind_change.apply(persy_impl, &mut self)?;
        }

        let (records, crt_upd_segs, dropped_segs) = self.coll_locks();
        let check_version = self.strategy != TxStrategy::LastWin;
        let old_records = match address.acquire_locks(&records, &crt_upd_segs, &dropped_segs, check_version) {
            Ok(old_records) => old_records,
            Err(x) => {
                self.rollback_prepared(journal, address, indexes, allocator)?;
                return Err(x);
            }

        };

        let free = FreedRecordPages::new(old_records);
        journal.prepare(&free, &self.id)?;
        self.freed_pages = Some(free);
        journal.prepare(&PrepareCommit::new(), &self.id)?;
        Ok(self)
    }

    fn coll_locks(&self) -> (Vec<(u32, RecRef, u16)>, Vec<u32>, Vec<u32>) {
        let mut crt_upd_segs = Vec::new();
        for create in &self.segs_created {
            if !&self.segs_dropped.contains(create) {
                crt_upd_segs.push(create.clone());
            }
        }
        for update in &self.segs_updated {
            if !&self.segs_dropped.contains(update) {
                crt_upd_segs.push(update.clone());
            }
        }

        let mut dropped_segs = Vec::new();
        for dropped in &self.segs_dropped {
            dropped_segs.push(dropped.clone());
        }
        let mut records = HashSet::new();

        // No need to lock on inserted records the new id unique is managed by the address.
        //
        for update in &self.updated {
            let mut version = update.version;
            // I found values in the read only for VersionOnRead
            if let Some(read_v) = self.read.get(&update.recref) {
                version = read_v.version;
            }
            records.insert((update.segment.clone(), update.recref.clone(), version));
        }

        for delete in &self.deleted {
            let mut version = delete.version;
            // I found values in the read only for VersionOnRead
            if let Some(read_v) = self.read.get(&delete.recref) {
                version = read_v.version;
            }
            records.insert((delete.segment.clone(), delete.recref.clone(), version));
        }

        for insert in &self.inserted {
            records.remove(&(insert.segment.clone(), insert.recref.clone(), 1));
        }

        let mut sorted_records: Vec<(u32, RecRef, u16)> = records.iter().cloned().collect();
        sorted_records.sort_by_key(|ref x| x.1.clone());
        crt_upd_segs.sort();
        dropped_segs.sort();
        (sorted_records, crt_upd_segs, dropped_segs)
    }

    fn internal_rollback(&self, address: &Address, allocator: &Allocator) -> PRes<()> {
        let mut dropped_segs = Vec::new();
        for create in &self.segs_created {
            dropped_segs.push(create.clone());
        }
        for insert in &self.inserted {
            address.rollback(&insert.recref)?;
            if dropped_segs.contains(&insert.segment) {
                allocator.free(insert.record_page)?;
            }
        }

        for create in &self.segs_created {
            address.drop_temp_segment(*create)?;
        }

        for update in &self.updated {
            if dropped_segs.contains(&update.segment) {
                allocator.free(update.record_page)?;
            }
        }
        Ok(())
    }

    pub fn recover_rollback(&self, journal: &Journal, address: &Address, allocator: &Allocator) -> PRes<()> {
        journal.end(&Rollback::new(), &self.id)?;
        self.internal_rollback(address, allocator)
    }

    pub fn rollback(&mut self, journal: &Journal, address: &Address, allocator: &Allocator) -> PRes<()> {
        let clear_journal = journal.end(&Rollback::new(), &self.id)?;

        self.internal_rollback(address, allocator)?;

        if clear_journal {
            journal.clear(&self.id)?;
        }

        allocator.flush_free_list()?;
        Ok(())
    }

    pub fn rollback_prepared(&mut self, journal: &Journal, address: &Address, indexes: &Indexes, allocator: &Allocator) -> PRes<()> {
        let clear_journal = journal.end(&Rollback::new(), &self.id)?;

        for insert in &self.inserted {
            address.rollback(&insert.recref)?;
        }
        //TODO: Double check the order of release locks and internal rollback.
        let (records, crt_upd_segs, deleted_segs) = self.coll_locks();
        address.release_locks(records, &crt_upd_segs, &deleted_segs)?;

        if let Some(il) = &self.locked_indexes {
            indexes.write_unlock(il)?;
        }

        self.internal_rollback(address, allocator)?;

        if clear_journal {
            journal.clear(&self.id)?;
        }
        allocator.flush_free_list()?;
        Ok(())
    }

    pub fn recover_commit(&mut self, journal: &Journal, address: &Address, indexes: &Indexes, allocator: &Allocator) -> PRes<()> {
        self.internal_commit(address, indexes, allocator, true)?;
        journal.end(&Commit::new(), &self.id)?;
        Ok(())
    }

    fn internal_commit(&mut self, address: &Address, indexes: &Indexes, allocator: &Allocator, recover: bool) -> PRes<()> {
        address.apply(
            &self.inserted,
            &self.updated,
            &self.deleted,
            &self.segments_operations,
            recover,
        )?;
        let (records, crt_upd_segs, deleted_segs) = self.coll_locks();
        address.release_locks(records, &crt_upd_segs, &deleted_segs)?;

        if let Some(il) = &self.locked_indexes {
            indexes.write_unlock(il)?;
        }

        let segs: HashSet<u32> = deleted_segs.into_iter().collect();

        //Free update old pages
        if let Some(ref up_free) = self.freed_pages {
            for to_free in &up_free.pages {
                if segs.contains(&to_free.0) {
                    allocator.free(to_free.1)?;
                }
            }
        }

        for insert in &self.inserted {
            if segs.contains(&insert.segment) {
                allocator.free(insert.record_page)?;
            }
        }

        for update in &self.updated {
            if segs.contains(&update.segment) {
                allocator.free(update.record_page)?;
            }
        }

        Ok(())
    }

    pub fn commit(&mut self, address: &Address, journal: &Journal, indexes: &Indexes, allocator: &Allocator) -> PRes<()> {
        self.internal_commit(address, indexes, allocator, false)?;
        let clear_journal = journal.end(&Commit::new(), &self.id)?;
        if clear_journal {
            journal.clear(&self.id)?;
        }
        allocator.flush_free_list()?;
        Ok(())
    }

    pub fn recover_metadata(&mut self, metadata: &Metadata) {
        self.strategy = metadata.strategy.clone();
        self.meta_id = metadata.meta_id.clone();
    }

    pub fn recover_freed_pages(&mut self, freed: &FreedRecordPages) {
        self.freed_pages = Some((*freed).clone());
    }

    pub fn meta_id<'a>(&'a self) -> &'a Vec<u8> {
        &self.meta_id
    }
}

impl DeleteRecord {
    pub fn new(segment: u32, rec_ref: &RecRef, old_record: u64, version: u16) -> DeleteRecord {
        DeleteRecord {
            segment: segment,
            recref: rec_ref.clone(),
            record_old_page: old_record,
            version: version,
        }
    }
}

impl UpdateRecord {
    pub fn new(segment: u32, rec_ref: &RecRef, record: u64, old_record: u64, version: u16) -> UpdateRecord {
        UpdateRecord {
            segment: segment,
            recref: rec_ref.clone(),
            record_page: record,
            record_old_page: old_record,
            version: version,
        }
    }
}

impl ReadRecord {
    pub fn new(segment: u32, recref: &RecRef, version: u16) -> ReadRecord {
        ReadRecord {
            segment: segment,
            recref: recref.clone(),
            version: version,
        }
    }
}

impl PrepareCommit {
    pub fn new() -> PrepareCommit {
        PrepareCommit {}
    }
}

impl Commit {
    pub fn new() -> Commit {
        Commit {}
    }
}
impl Rollback {
    pub fn new() -> Rollback {
        Rollback {}
    }
}

impl InsertRecord {
    pub fn new(segment: u32, rec_ref: &RecRef, record: u64) -> InsertRecord {
        InsertRecord {
            segment: segment,
            recref: rec_ref.clone(),
            record_page: record,
        }
    }
}

impl CreateSegment {
    pub fn new(name: &str, segment_id: u32) -> CreateSegment {
        CreateSegment {
            name: name.into(),
            segment_id: segment_id,
        }
    }
}

impl DropSegment {
    pub fn new(name: &str, segment_id: u32) -> DropSegment {
        DropSegment {
            name: name.into(),
            segment_id: segment_id,
        }
    }
}

impl Metadata {
    pub fn new(strategy: &TxStrategy, meta_id: Vec<u8>) -> Metadata {
        Metadata {
            strategy: strategy.clone(),
            meta_id: meta_id,
        }
    }
}

impl FreedRecordPages {
    pub fn new(pages: Vec<(u32, u64)>) -> FreedRecordPages {
        FreedRecordPages { pages: pages }
    }
}


#[cfg(test)]
mod tests {

    use transaction::{Transaction, InsertRecord};
    use journal::JournalId;
    use persy::RecRef;

    #[test()]
    fn test_scan_insert() {
        let mut tx = Transaction::recover(JournalId::new(0, 0));
        tx.inserted.push(
            InsertRecord::new(10, &RecRef::new(3, 2), 2),
        );
        tx.inserted.push(
            InsertRecord::new(10, &RecRef::new(4, 2), 2),
        );
        tx.inserted.push(
            InsertRecord::new(20, &RecRef::new(0, 1), 3),
        );
        let mut count = 0;
        for x in tx.scan_insert(10) {
            assert_eq!(x.pos, 2);
            count += 1;
        }
        assert_eq!(count, 2);
    }

}
