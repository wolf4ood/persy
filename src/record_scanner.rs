use PersyId;
use persy::PersyImpl;
use transaction::Transaction;
use segment::{SegmentScanner, SegmentIterator};
use std::iter::Chain;
use transaction::TransactionInsertIterator;

/// IntoIterator of persistent records of a segment
pub struct RecordScanner<'a> {
    persy: &'a PersyImpl,
    segment_id: u32,
    scanner: SegmentScanner<'a>,
}

pub struct RecordIterator<'a> {
    persy: &'a PersyImpl,
    segment_id: u32,
    iterator: SegmentIterator<'a>,
}

/// Snapshot of a record from the segment iterator
pub struct IteratorItem {
    /// Identifier of the record can be used for do update or delete.
    ///
    pub id: PersyId,
    /// Content of the record
    ///
    pub content: Vec<u8>,
}

impl<'a> RecordScanner<'a> {
    pub fn new<'b>(persy: &'b PersyImpl, segment_id: u32, scanner: SegmentScanner<'b>) -> RecordScanner<'b> {
        RecordScanner::<'b> {
            persy: persy,
            segment_id: segment_id,
            scanner: scanner,
        }
    }
}

impl<'a> IntoIterator for RecordScanner<'a> {
    type Item = IteratorItem;
    type IntoIter = RecordIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        RecordIterator::<'a> {
            persy: self.persy,
            segment_id: self.segment_id,
            iterator: self.scanner.into_iter(),
        }
    }
}

impl<'a> Iterator for RecordIterator<'a> {
    type Item = IteratorItem;
    fn next(&mut self) -> Option<Self::Item> {
        let rec;
        loop {
            let iter = self.iterator.next();
            if let Some(id) = iter {
                let res = self.persy.read_record_scan(self.segment_id, &id);
                if !res.is_err() {
                    let tp = res.unwrap();
                    if let Some(val) = tp {
                        rec = Some(IteratorItem {
                            id: id,
                            content: val,
                        });
                        break;
                    }
                }
            } else {
                rec = None;
                break;
            }
        }

        rec
    }
}

/// IntoIterator of persistent records of a segment that include eventual in transaction changes
pub struct RecordScannerTx<'a> {
    persy: &'a PersyImpl,
    segment_id: u32,
    scanner: SegmentScanner<'a>,
    tx: &'a Transaction,
}

pub struct RecordIteratorTx<'a> {
    persy: &'a PersyImpl,
    segment_id: u32,
    iterator: Chain<TransactionInsertIterator, SegmentIterator<'a>>,
    tx: &'a Transaction,
}

impl<'a> RecordScannerTx<'a> {
    pub fn new<'b>(persy: &'b PersyImpl, tx: &'b Transaction, segment_id: u32, scanner: SegmentScanner<'b>) -> RecordScannerTx<'b> {
        RecordScannerTx::<'b> {
            persy: persy,
            segment_id: segment_id,
            scanner: scanner,
            tx: tx,
        }
    }
}

impl<'a> IntoIterator for RecordScannerTx<'a> {
    type Item = IteratorItem;
    type IntoIter = RecordIteratorTx<'a>;

    fn into_iter(self) -> Self::IntoIter {
        let iter = self.tx.scan_insert(self.segment_id).into_iter().chain(
            self.scanner.into_iter(),
        );
        RecordIteratorTx::<'a> {
            persy: self.persy,
            segment_id: self.segment_id,
            iterator: iter,
            tx: self.tx,
        }
    }
}

impl<'a> Iterator for RecordIteratorTx<'a> {
    type Item = IteratorItem;
    fn next(&mut self) -> Option<Self::Item> {
        let rec;
        loop {
            let iter = self.iterator.next();
            if let Some(id) = iter {
                let res = self.persy.read_record_scan_tx(
                    &self.tx,
                    self.segment_id,
                    &id,
                );
                if !res.is_err() {
                    let tp = res.unwrap();
                    if let Some(val) = tp {
                        rec = Some(IteratorItem {
                            id: id.clone(),
                            content: val,
                        });
                        break;
                    }
                }
            } else {
                rec = None;
                break;
            }
        }

        rec
    }
}
