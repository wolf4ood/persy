extern crate linked_hash_map;

use persy::PRes;
use discref::{DiscRef, PageSeek};
use discref::Page;
use std::io::Read;
use std::io;
use byteorder::{ReadBytesExt, WriteBytesExt, BigEndian};
use std::sync::Mutex;
use self::linked_hash_map::LinkedHashMap;
use config::Config;

const ALLOCATOR_PAGE_EXP: u8 = 9; // 2^9

pub struct Cache {
    cache: LinkedHashMap<u64, Page>,
    size: u64,
    limit: u64,
}

impl Cache {
    pub fn new(limit: u64) -> Cache {
        Cache {
            cache: LinkedHashMap::new(),
            size: 0,
            limit: limit,
        }
    }
    fn get(&mut self, key: &u64) -> Option<Page> {
        if let Some(val) = self.cache.get_refresh(key) {
            return Some(val.clone());
        }
        None
    }
    fn put(&mut self, key: u64, value: &Page) {
        self.size = self.size + (1 << value.get_size_exp());
        self.cache.insert(key, value.clone());
        loop {
            if self.size > self.limit {
                let rem = self.cache.pop_front();
                if let Some(en) = rem {
                    self.size = self.size - (1 << en.1.get_size_exp());
                } else {
                    break;
                }
            } else {
                break;
            }
        }
    }
}


// TODO: Manage defragmentation by merging/splitting pages in the free list
pub struct Allocator {
    disc: DiscRef,
    freelist: Mutex<[u64; 32]>,
    cache: Mutex<Cache>,
    page: u64,
}

pub struct ReadPage {
    page: Page,
}

impl ReadPage {
    pub fn get_size_exp(&mut self) -> u8 {
        self.page.get_size_exp()
    }
}

impl Read for ReadPage {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.page.read(buf)
    }
}

impl PageSeek for ReadPage {
    fn seek(&mut self, pos: u32) -> PRes<()> {
        self.page.seek(pos)
    }
}


impl Allocator {
    pub fn new(dr: DiscRef, config: &Config, page: u64) -> PRes<Allocator> {
        let mut freelist = [0; 32];
        {
            let mut pag = dr.load_page(page)?;
            for p in &mut freelist {
                (*p) = pag.read_u64::<BigEndian>()?;
            }
        }
        let cache_size = config.cache_size();
        Ok(Allocator {
            disc: dr,
            freelist: Mutex::new(freelist),
            cache: Mutex::new(Cache::new(cache_size)),
            page: page,
        })
    }

    pub fn init(dr: &DiscRef) -> PRes<u64> {
        dr.create_page(ALLOCATOR_PAGE_EXP)
    }

    pub fn load_page(&self, page: u64) -> PRes<ReadPage> {
        Ok(ReadPage { page: self.write_page(page)? })
    }

    pub fn write_page(&self, page: u64) -> PRes<Page> {
        {
            let mut cache = self.cache.lock()?;

            if let Some(pg) = cache.get(&page) {
                return Ok(pg);
            }
        }
        let load = self.disc.load_page(page)?;
        {
            let mut cache = self.cache.lock()?;
            cache.put(page, &load);
        }
        Ok(load)
    }

    pub fn allocate(&self, exp: u8) -> PRes<u64> {
        {
            let mut fl = self.freelist.lock()?;
            let page = fl[exp as usize];
            if page != 0 as u64 {
                let next;
                {
                    let mut pg = self.write_page(page)?;
                    next = pg.get_next_free()?;
                }
                fl[exp as usize] = next;
                return Ok(page);
            }
        }
        return self.disc.create_page(exp);
    }

    pub fn flush_page(&self, page: &mut Page) -> PRes<()> {
        self.disc.flush_page(page)?;
        {
            let mut cache = self.cache.lock()?;
            cache.put(page.get_index(), page);
        }
        Ok(())
    }

    pub fn remove_from_free(&self, page: u64, exp: u8) -> PRes<()> {
        //TODO: this at the moment may leak free pages on recover after crash
        let mut fl = self.freelist.lock()?;
        if fl[exp as usize] == page {
            fl[exp as usize] = 0;
        } else {
            let mut p = fl[exp as usize];
            while p != 0 as u64 {
                let mut pg = self.write_page(p)?;
                p = pg.get_next_free()?;
                if p == page {
                    pg.set_next_free(0)?;
                    self.flush_page(&mut pg)?;
                    break;
                }
            }
        }
        Ok(())
    }

    pub fn free(&self, page: u64) -> PRes<()> {
        let mut pag = self.disc.load_page(page)?;
        let size = pag.get_size_exp();
        {
            let mut fl = self.freelist.lock()?;
            let old = fl[size as usize];
            debug_assert!(old != page, "freeing: {} already free: {} ", page, old);
            fl[size as usize] = page;
            pag.set_next_free(old)?;
            self.flush_page(&mut pag)?;
        }
        Ok(())
    }

    pub fn flush_free_list(&self) -> PRes<()> {
        let mut pag = self.disc.load_page(self.page)?;
        for p in &(*self.freelist.lock()?) {
            pag.write_u64::<BigEndian>(*p)?;
        }
        self.disc.flush_page(&mut pag)?;
        Ok(())
    }

    pub fn disc<'a>(&'a self) -> &'a DiscRef {
        &self.disc
    }
}


#[cfg(test)]
mod tests {

    extern crate tempfile;
    use self::tempfile::Builder;
    use std::io::Cursor;
    use discref::Page;
    use discref::DiscRef;
    use super::Allocator;
    use super::Cache;
    use std::rc::Rc;
    use config::Config;

    #[test]
    fn test_reuse_freed_page() {
        let file = Builder::new()
            .prefix("all_reuse_test")
            .suffix(".persy")
            .tempfile()
            .unwrap()
            .reopen()
            .unwrap();
        let disc = DiscRef::new(file);
        let pg = Allocator::init(&disc).unwrap();
        let allocator = Allocator::new(disc, &Rc::new(Config::new()), pg).unwrap();
        // This is needed to avoid the 0 page
        allocator.allocate(2).unwrap();
        let first = allocator.allocate(10).unwrap();
        let second = allocator.allocate(10).unwrap();
        let third = allocator.allocate(11).unwrap();

        allocator.free(first).unwrap();
        allocator.free(second).unwrap();
        allocator.free(third).unwrap();

        let val = allocator.allocate(10).unwrap();
        assert_eq!(val, second);
        let val = allocator.allocate(10).unwrap();
        assert_eq!(val, first);
        let val = allocator.allocate(10).unwrap();
        assert!(val != first);
        assert!(val != second);
        let val = allocator.allocate(11).unwrap();
        assert_eq!(val, third);
        let val = allocator.allocate(11).unwrap();
        assert!(val != third);
    }

    #[test]
    fn test_remove_freed_page() {

        let file = Builder::new()
            .prefix("remove_free_test")
            .suffix(".persy")
            .tempfile()
            .unwrap()
            .reopen()
            .unwrap();
        let disc = DiscRef::new(file);
        let pg = Allocator::init(&disc).unwrap();
        let allocator = Allocator::new(disc, &Rc::new(Config::new()), pg).unwrap();
        // This is needed to avoid the 0 page
        allocator.allocate(2).unwrap();
        let first = allocator.allocate(10).unwrap();
        let second = allocator.allocate(10).unwrap();
        let third = allocator.allocate(10).unwrap();

        allocator.free(first).unwrap();
        allocator.free(second).unwrap();
        allocator.free(third).unwrap();
        allocator.remove_from_free(second, 10).unwrap();
        let val = allocator.allocate(10).unwrap();
        assert_eq!(val, third);
        let val = allocator.allocate(10).unwrap();
        assert!(val != first);
        assert!(val != second);
        assert!(val != third);
    }


    #[test]
    fn test_cache_limit_evict() {
        let mut cache = Cache::new(1050 as u64);
        cache.put(10, &Page::new(Cursor::new(Vec::new()), 10, 9));
        cache.put(20, &Page::new(Cursor::new(Vec::new()), 10, 9));
        cache.put(30, &Page::new(Cursor::new(Vec::new()), 10, 9));
        assert!(cache.size < 1050);
        assert_eq!(cache.cache.len(), 2);
        let ten = 10 as u64;
        match cache.get(&ten) {
            Some(_) => assert!(false),
            None => assert!(true),
        }
        let ten = 20 as u64;
        match cache.get(&ten) {
            Some(_) => assert!(true),
            None => assert!(false),
        }
        let ten = 30 as u64;
        match cache.get(&ten) {
            Some(_) => assert!(true),
            None => assert!(false),
        }
    }

    #[test]
    fn test_cache_limit_refresh_evict() {
        let mut cache = Cache::new(1050 as u64);
        cache.put(10, &Page::new(Cursor::new(Vec::new()), 10, 9));
        cache.put(20, &Page::new(Cursor::new(Vec::new()), 10, 9));
        let ten = 10 as u64;
        match cache.get(&ten) {
            Some(_) => assert!(true),
            None => assert!(false),
        }

        cache.put(30, &Page::new(Cursor::new(Vec::new()), 10, 9));
        assert!(cache.size < 1050);
        assert_eq!(cache.cache.len(), 2);
        let ten = 10 as u64;
        match cache.get(&ten) {
            Some(_) => assert!(true),
            None => assert!(false),
        }
        let ten = 20 as u64;
        match cache.get(&ten) {
            Some(_) => assert!(false),
            None => assert!(true),
        }
        let ten = 30 as u64;
        match cache.get(&ten) {
            Some(_) => assert!(true),
            None => assert!(false),
        }
    }

}
