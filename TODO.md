
# TO DO
In case of drop segment transform in delete all the create/update record operation in transaction for the relative segment
In case of recover make sure that none of the pages recovered are present in the free list

## Features

# Tests TO DO

## Multithread
manipolate record of segment while another thread drop the segment

## Allocation
Add test case for check if disk space is returned correctly after drop segment

## Scan Transactions
Test failure of scan of a segment created in another tx

# Code cleanup
check for in tests module refer and remove double implementation of CountDown

# Optimizations
Remove segment check in prepare-commit record locks



